#pragma once
#define WIN32_LEAN_AND_MEAN
#include "DXUtility.h"

class DXApp
{
protected:
	//Win32 Attributes
	bool						mWindowed;
	bool						mVsyncEnabled;
	HWND						mApphWnd;
	HINSTANCE					mAppInstance;
	UINT						mClientWidth, mClientHeight;
	std::string					mAppTitle;
	DWORD						mWndStyle;

	//Directx Attributes
	int										mVideoCardMemory;
	char									mVideoCardDescription[128];
	ID3D11Device							*mDevice;
	ID3D11DeviceContext						*mImmediateContext;
	IDXGISwapChain							*mSwapChain;
	ID3D11RenderTargetView					*mRenderTargetView;
	ID3D11Texture2D							*mDepthStencil;
	ID3D11DepthStencilState					*mDepthStencilState;
	ID3D11DepthStencilView					*mDepthStencilView;
	ID3D11RasterizerState					*mRasterizerState;
	DirectX::SpriteBatch					*mSpriteBatch;
	DirectX::SpriteFont						*mSpriteFont;
	D3D_DRIVER_TYPE							mDriverType;
	D3D_FEATURE_LEVEL						mFeatureLevel;
	D3D11_VIEWPORT							mViewport;

public:
	DXApp(bool windowed, bool vsync, HINSTANCE hInstance, UINT width, UINT height, std::string title);
	virtual ~DXApp();

	//MAIN
	POSITIVEINT Run();

	//Framework
	virtual bool Initialize(const wchar_t* fontFile);
	virtual void Update() = 0;
	virtual void Render() = 0;
	virtual LRESULT MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	virtual void Release();

protected:
	//Initialize Win32 Window
	bool InitializeWindow();

	//Initialize Directx
	bool InitializeD3D(const wchar_t *fontFile);
};
