#pragma once
#include "DXApp.h"
#include "DirectInput.h"
#include "DirectShow.h"
#include "GameTime.h"
#include "Skybox.h"
#include "Terrain.h"
#include "GameCamera.h"
#include "ShaderManager.h"
#include "QuadTree.h"
#include "LightSource.h"
//#include "Water.h"

class GameApp : public DXApp
{
private:
	GameCamera *mCamera;
	TextureManager *mTexManager;
	GameTime *mTime;
	DirectInput *mInput;
	DirectShow *mShow;
	ShaderManager *mShaderManager;
	Terrain *mTerrain;
	TerrainShader *mTerrainShader;
	QuadTree *mQuadTree;
	Frustrum *mFrustrum;
	Skybox *mSkybox;
	LightSource *mSun;

	std::vector<TSVEntity*> mEntityVec;

public:
	GameApp(bool windowed, bool vSync, HINSTANCE hInstance, UINT width, UINT height, std::string title);
	~GameApp();

	bool Initialize(const wchar_t* file);
	void Update();
	void Render();
	void CheckInput();
	void Release();

	POSITIVEINT GetNextValidId();
};