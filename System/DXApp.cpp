#include "DXApp.h"

namespace
{
	//Used to forward msg to user defined proc function
	DXApp* g_dxApp = nullptr;
}

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (g_dxApp)
		return g_dxApp->MsgProc(hWnd, msg, wParam, lParam);
	else
		return DefWindowProc(hWnd, msg, wParam, lParam);
}

DXApp::DXApp(bool windowed, bool vsync, HINSTANCE hInstance, UINT width, UINT height, std::string title)
{
	mWindowed = windowed;
	mVsyncEnabled = vsync;
	mAppInstance = hInstance;
	mApphWnd = NULL;
	mClientHeight = height;
	mClientWidth = width;
	mAppTitle = title;
	mWndStyle = WS_OVERLAPPEDWINDOW;
	g_dxApp = this;

	mDevice = nullptr;
	mImmediateContext = nullptr;
	mRenderTargetView = nullptr;
	mSwapChain = nullptr;
	mDepthStencil = nullptr;
	mDepthStencilState = nullptr;
	mDepthStencilView = nullptr;
	mRasterizerState = nullptr;
}

DXApp::~DXApp()
{
	if (mSwapChain)
		mSwapChain->SetFullscreenState(false, NULL);

	if (mRasterizerState)
		Memory::SafeRelease(mRasterizerState);

	if (mDepthStencilView)
		Memory::SafeRelease(mDepthStencilView);

	if (mDepthStencilState)
		Memory::SafeRelease(mDepthStencilState);

	if (mDepthStencil)
		Memory::SafeRelease(mDepthStencil);

	if (mRenderTargetView)
		Memory::SafeRelease(mRenderTargetView);

	if (mImmediateContext)
	{
		mImmediateContext->ClearState();
		Memory::SafeRelease(mImmediateContext);
	}

	if (mDevice)
		Memory::SafeRelease(mDevice);

	if (mSwapChain)
		Memory::SafeRelease(mSwapChain);
}

POSITIVEINT DXApp::Run()
{
	//Main Message Loop
	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			Update();

			Render();
		}
	}

	return static_cast<int>(msg.wParam);
}

bool DXApp::Initialize(const wchar_t* fontFile)
{
	if (!InitializeWindow())
		return false;

	if (!InitializeD3D(fontFile))
		return false;

	return true;
}

bool DXApp::InitializeD3D(const wchar_t *fontFile)
{
	POSITIVEINT numModes, numerator, denominator, stringLength;

	IDXGIFactory *factory;
	HR(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory));

	IDXGIAdapter *adapter;
	HR(factory->EnumAdapters(0, &adapter));

	IDXGIOutput *adapterOutput;
	HR(adapter->EnumOutputs(0, &adapterOutput));

	HR(adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL));
	DXGI_MODE_DESC *displayModeList = new DXGI_MODE_DESC[numModes];
	if (!displayModeList)
		return false;

	HR(adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList));

	for (POSITIVEINT i = 0; i < numModes; i++)
	{
		if (displayModeList[i].Width == (POSITIVEINT)mClientWidth)
		{
			if (displayModeList[i].Height == (POSITIVEINT)mClientHeight)
			{
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}
	}

	DXGI_ADAPTER_DESC adapterDesc;
	HR(adapter->GetDesc(&adapterDesc));

	mVideoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

	WHOLENUM error = wcstombs_s(&stringLength, mVideoCardDescription, 128, adapterDesc.Description, 128);
	if (error != 0)
		return false;

	Memory::SafeDeleteArr(displayModeList);

	Memory::SafeRelease(adapterOutput);

	Memory::SafeRelease(adapter);

	Memory::SafeRelease(factory);

	DXGI_SWAP_CHAIN_DESC swapDesc;
	ZeroMemory(&swapDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	swapDesc.BufferCount = 1; //single buffered
	swapDesc.BufferDesc.Width = mClientWidth;
	swapDesc.BufferDesc.Height = mClientHeight;
	swapDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	if (mVsyncEnabled)
	{
		swapDesc.BufferDesc.RefreshRate.Numerator = numerator;
		swapDesc.BufferDesc.RefreshRate.Denominator = denominator;
	}
	else
	{
		swapDesc.BufferDesc.RefreshRate.Numerator = 1;
		swapDesc.BufferDesc.RefreshRate.Denominator = 0;
	}

	//Set the usage of the back buffer
	swapDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	//Set handle to window
	swapDesc.OutputWindow = mApphWnd;
	//Discard back buffer contents after presenting
	swapDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapDesc.Windowed = mWindowed;
	//No flags
	swapDesc.Flags = 0;
	//Turn off multi-sampling
	swapDesc.SampleDesc.Count = 1;
	swapDesc.SampleDesc.Quality = 0;
	//Set scan line ordering and scaling
	swapDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	//Set feature level to DirectX 11
	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;

	HR(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, &featureLevel, 1, D3D11_SDK_VERSION, &swapDesc, &mSwapChain, &mDevice, NULL, &mImmediateContext));

	mSpriteBatch = new DirectX::SpriteBatch(mImmediateContext);
	mSpriteFont = new DirectX::SpriteFont(mDevice, fontFile);

	ID3D11Texture2D *backBuffer;
	HR(mSwapChain->GetBuffer(NULL, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer)));
	HR(mDevice->CreateRenderTargetView(backBuffer, nullptr, &mRenderTargetView));

	Memory::SafeRelease(backBuffer);

	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

	depthBufferDesc.Width = mClientWidth;
	depthBufferDesc.Height = mClientHeight;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	HR(mDevice->CreateTexture2D(&depthBufferDesc, NULL, &mDepthStencil));

	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));

	//Set up depth stencil desc
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	//Stencil operations for when pixel is facing back
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	//Stencil operations for pixel facing front
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//Create depth stencil state
	HR(mDevice->CreateDepthStencilState(&depthStencilDesc, &mDepthStencilState));
	//Set depth stencil state
	mImmediateContext->OMSetDepthStencilState(mDepthStencilState, 1);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	//Set depth stencil view desc
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	//Create depth stencil view
	HR(mDevice->CreateDepthStencilView(mDepthStencil, &depthStencilViewDesc, &mDepthStencilView));

	//Bind render target view and depth stencil buffer to pipeline
	mImmediateContext->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);

	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.AntialiasedLineEnable = false;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;

	//Create rasterizer state
	HR(mDevice->CreateRasterizerState(&rasterizerDesc, &mRasterizerState));

	//Viewport Creation
	mViewport.Width = static_cast<FLOAT>(mClientWidth);
	mViewport.Height = static_cast<FLOAT>(mClientHeight);
	mViewport.TopLeftX = 0.0f;
	mViewport.TopLeftY = 0.0f;
	mViewport.MinDepth = 0.0f;
	mViewport.MaxDepth = 1.0f;

	//Bind viewport
	mImmediateContext->RSSetViewports(1, &mViewport);

	return true;
}

bool DXApp::InitializeWindow()
{
	//Window Desc
	WNDCLASSEX wcex;
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.hInstance = mAppInstance;
	wcex.lpfnWndProc = MainWndProc;
	wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = "DXAPPWNDCLASS";
	wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wcex))
	{
		OutputDebugString("/nFAILED TO CREATE WINDOW CLASS/n");
		return false;
	}

	RECT r = { 0, 0, mClientWidth, mClientHeight };
	AdjustWindowRect(&r, mWndStyle, FALSE);
	UINT width = r.right - r.left;
	UINT height = r.bottom - r.top;

	UINT x = GetSystemMetrics(SM_CXSCREEN) / 2 - width / 2;
	UINT y = GetSystemMetrics(SM_CYSCREEN) / 2 - height / 2;

	mApphWnd = CreateWindow("DXAPPWNDCLASS", mAppTitle.c_str(), mWndStyle, x, y, width, height, NULL, NULL, mAppInstance, NULL);

	if (!mApphWnd)
	{
		OutputDebugString("/nFAILED TO CREATE WINDOW/n");
		return false;
	}

	ShowWindow(mApphWnd, SW_SHOW);

	return true;
}

void DXApp::Release()
{
	if (mSwapChain)
		mSwapChain->SetFullscreenState(false, NULL);

	if (mRasterizerState)
		Memory::SafeRelease(mRasterizerState);

	if (mDepthStencilView)
		Memory::SafeRelease(mDepthStencilView);

	if (mDepthStencilState)
		Memory::SafeRelease(mDepthStencilState);

	if (mDepthStencil)
		Memory::SafeRelease(mDepthStencil);

	if (mRenderTargetView)
		Memory::SafeRelease(mRenderTargetView);

	if (mImmediateContext)
	{
		mImmediateContext->ClearState();
		Memory::SafeRelease(mImmediateContext);
	}

	if (mDevice)
		Memory::SafeRelease(mDevice);

	if (mSwapChain)
		Memory::SafeRelease(mSwapChain);
}

LRESULT DXApp::MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}