#include "GameApp.h"

int WINAPI WinMain(__in HINSTANCE hInstance, __in_opt HINSTANCE hPrevInstance, __in LPSTR lpCmdLine, __in int nShowCmd)
{
	GameApp* gApp = new GameApp(false, false, hInstance, 1260, 750, "TSVS Engine");

	if (!gApp->Initialize(L"Arial.spritefont"))
		return 1;

	return gApp->Run();
}