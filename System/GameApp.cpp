#include "GameApp.h"
#include "TextureShader.h"
#include "ColorShader.h"
#include "SkyboxShader.h"
#include "TerrainShader.h"
#include "WaterShader.h"
#include "Pyramid.h"
#include "Grid.h"
#include "Cube.h"
#include "MenuFSM.h"
#include "LoadingMenuState.h"

GameApp::GameApp(bool windowed, bool vSync, HINSTANCE hInstance, UINT width, UINT height, std::string title)
	: DXApp(windowed, vSync, hInstance, width, height, title)
{
}

GameApp::~GameApp()
{
	Release();

	CoUninitialize();
}

bool GameApp::Initialize(const wchar_t* file)
{
	if (!DXApp::Initialize(file))
		return false;

	mTexManager = new TextureManager();

	mTexManager->Load2DTexture(L"StoneGrass.dds", mDevice);
	mTexManager->Load2DTexture(L"Skymap02.dds", mDevice);
	mTexManager->Load2DTexture(L"Grass.dds", mDevice);
	mTexManager->Load2DTexture(L"Stone.dds", mDevice);
	mTexManager->Load2DTexture(L"Water.dds", mDevice);

	mShaderManager = new ShaderManager();
	mInput = new DirectInput(mAppInstance, mApphWnd);
	
	mShow = new DirectShow();
	if (!mShow->Initialize())
		return false;

	mShow->PlayVideo(mApphWnd, L"C:\\Users\\Trent\\Documents\\Visual Studio 2013\\Projects\\DX11Game\\DX11Game\\TSVS_Intro_03.wmv", mInput);

	MenuFSM *tempMenuFSM = new MenuFSM();
	if (!tempMenuFSM->Initialize(new LoadingMenuState()))
		return false;

	//RECT screen;
	//GetClientRect(mApphWnd, &screen);

	//std::thread loadThread(&MenuFSM::Update, tempMenuFSM, screen, mDevice, mRenderTargetView, mSwapChain, mSpriteBatch, mSpriteFont);
	
	mCamera = new GameCamera();
	if (!mCamera->Initialize(VECTOR3(0.0f, 5.0f, 0.0f), mApphWnd))
		return false;

	mTime = new GameTime();
	if (!mTime->Initialize())
		return false;

	mTerrainShader = new TerrainShader(L"TerrainVertexShader.hlsl", L"TerrainPixelShader.hlsl");
	if (!mTerrainShader->InitializeShader(mDevice, mApphWnd))
		return false;
	else
		mShaderManager->LoadShader(L"TerrainShader", mTerrainShader);

	TextureShader *texShader = new TextureShader(L"TextureVertexShader.hlsl", L"TexturePixelShader.hlsl");
	if (!texShader->InitializeShader(mDevice, mApphWnd))
		return false;
	else
		mShaderManager->LoadShader(L"TextureShader", texShader);

	ColorShader *colorShader = new ColorShader(L"ColorVertexShader.hlsl", L"ColorPixelShader.hlsl");
	if (!colorShader->InitializeShader(mDevice, mApphWnd))
		return false;
	else
		mShaderManager->LoadShader(L"ColorShader", colorShader);

	SkyboxShader *skyShader = new SkyboxShader(L"SkyboxVertexShader.hlsl", L"SkyboxPixelShader.hlsl");
	if (!skyShader->InitializeShader(mDevice, mApphWnd))
		return false;
	else
		mShaderManager->LoadShader(L"SkyboxShader", skyShader);

	WaterShader *waterShader = new WaterShader(L"WaterVertexShader.hlsl", L"WaterPixelShader.hlsl");
	if (!waterShader->InitializeShader(mDevice, mApphWnd))
		return false;
	else
		mShaderManager->LoadShader(L"WaterShader", waterShader);

	//////// D E B U G //////////////
	mTerrain = new Terrain(0, VECTOR3(0.0f, 0.0f, 0.0f), 1.0f);
	if (!mTerrain->Initialize(mDevice, L"Grass.dds", L"Stone.dds", L"StoneGrass.dds", mTexManager, "HeightMap.bmp", mShaderManager, L"TerrainShader"))
		return false;

	mFrustrum = new Frustrum(mShaderManager, mDevice);

	mQuadTree = new QuadTree();
	if (!mQuadTree->Initialize(mTerrain, mDevice))
		return false;

	mSkybox = new Skybox(NULL, mCamera->GetPosition(), 10, 10, 15.0f);
	if (!mSkybox->Initialize(mDevice, L"Skymap02.dds", mTexManager, mShaderManager, L"SkyboxShader"))
		return false;

	Cube *tempCube = new Cube(GetNextValidId(), VECTOR3(10.0f, 10.0f, 10.0f), 1.0f);
	if (!tempCube->Initialize(mDevice, L"Stone.dds", mTexManager, mShaderManager, L"TextureShader"))
		return false;
	else
		mEntityVec.push_back(tempCube);

	//Water *tempWater = new Water(0, VECTOR3(9779.0f, -2465.0f, 13253.0f), 1.0f, 1.0f, 1000.0f);
	//if (!tempWater->Initialize(mDevice, L"Stone.dds", L"Skymap02.dds", L"Water.dds", mTexManager, mShaderManager, L"WaterShader"))
		//return false;
	//else
		//mEntityVec.push_back(tempWater);

	mSun = new LightSource(VECTOR4(0.05f, 0.05f, 0.05f, 1.0f), VECTOR4(1.0f, 1.0f, 1.0f, 1.0f), VECTOR3(25000.0f, 20000.0f, 13000.0f), VECTOR3(12500.0f, -1000.0f, 6500.0f));
	///////////////////////////////////

	tempMenuFSM->SetFinished(true);
	//waterShader->SetCamWaterBuffers(mImmediateContext, mCamera->BuildReflectionMatrix(mCamera->GetPosition().y), mCamera->GetPosition(), tempWater->GetNormalTiling(), tempWater->GetWaterTranslation(), tempWater->GetReflectRefractScale(), tempWater->GetRefractionTint(), mSun->GetLightDirection(), tempWater->GetSpecularShine());
	//loadThread.join();

	return true;
}

void GameApp::Update()
{
	//Update Time
	mTime->Update();

	//Check Input
	mInput->CheckDevice(mApphWnd);
	CheckInput();

	//Update Camera
	mCamera->Update(mTime->GetDeltaTime(), mInput);

	mSkybox->SetPosition(mCamera->GetPreviousPosition());
	mSkybox->Update(mTime->GetDeltaTime());

	mTerrain->Update(mTime->GetDeltaTime());
	mTerrainShader->SetLightBuffer(mImmediateContext, mSun->GetAmbientColor(), mSun->GetDiffuseColor(), VECTOR3(0.0, -0.5, 0.5));

	if (!mEntityVec.empty())
	{
		for (POSITIVEINT i = 0; i < mEntityVec.size(); i++)
			mEntityVec[i]->Update(mTime->GetDeltaTime());
	}
}

void GameApp::Render()
{
	mImmediateContext->ClearRenderTargetView(mRenderTargetView, DirectX::Colors::White);
	mImmediateContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, NULL);

	if (!mEntityVec.empty())
	{
		for (POSITIVEINT i = 0; i < mEntityVec.size(); i++)
		{
			mEntityVec[i]->Render(mImmediateContext, mCamera->GetViewMatrix(), mCamera->GetProjectionMatrix(), mSun->GetLightViewMatrix(), mSun->GetLightProjectionMatrix());
		}
	}

	mFrustrum->ConstructFrustum(3000, mCamera->GetProjectionMatrix(), mCamera->GetViewMatrix());
	mQuadTree->Render(mFrustrum, mImmediateContext, mTerrainShader, mCamera, mTerrain, mSun);

	mSkybox->Render(mImmediateContext, mCamera->GetViewMatrix(), mCamera->GetProjectionMatrix(), mSun->GetLightViewMatrix(), mSun->GetLightProjectionMatrix());

	if (mVsyncEnabled)
	{
		HR(mSwapChain->Present(1, 0));
	}
	else
	{
		HR(mSwapChain->Present(0, 0));
	}
}

void GameApp::CheckInput()
{
	if (mInput->MousePressed(1))
	{
		POSITIVEINT centerX = mClientWidth / 2;
		POSITIVEINT centerY = mClientHeight / 2;

		RECT rec;
		GetWindowRect(mApphWnd, &rec);
		SetCursorPos(rec.right - centerX, rec.bottom - centerY);
	}
}

void GameApp::Release()
{
	DXApp::Release();

	if (mCamera)
		Memory::SafeDelete(mCamera);

	if (mTexManager)
		Memory::SafeDelete(mTexManager);

	if (mTime)
		Memory::SafeDelete(mTime);

	if (!mEntityVec.empty())
	{
		for (POSITIVEINT i = 0; i < mEntityVec.size(); i++)
			Memory::SafeRelease(mEntityVec[i]);
	}
}

POSITIVEINT GameApp::GetNextValidId()
{
	if (mEntityVec.empty())
		return 0;
	else
		return mEntityVec.size();
}