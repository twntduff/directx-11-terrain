#include "Skybox.h"

Skybox::Skybox(POSITIVEINT id, VECTOR3 position, FLOAT scale, POSITIVEINT latLines, POSITIVEINT longLines)
	: TSVEntity(id, position, scale)
{
	mLatitudeLines = latLines;
	mLongitudeLines = longLines;
}

Skybox::~Skybox()
{
	Release();
}

bool Skybox::Initialize(ID3D11Device *device, VECTOR4 color, ShaderManager *shaderManager, wchar_t* shaderFileName)
{
	mColor = color;

	if (!LoadShader(shaderManager, shaderFileName))
		return false;

	if (!InitializeBuffers(device))
		return false;

	return true;
}

bool Skybox::Initialize(ID3D11Device *device, wchar_t* fileName, TextureManager *texManager, ShaderManager *shaderManager, wchar_t* shaderFileName)
{
	if (!LoadShader(shaderManager, shaderFileName))
		return false;

	if (!LoadTexture(texManager, fileName))
		return false;

	if (!InitializeBuffers(device))
		return false;

	return true;
}

bool Skybox::InitializeBuffers(ID3D11Device *device)
{
	VertexPT *vertices;
	unsigned long *indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	/*POSITIVEINT numFaces = ((mLatitudeLines - 3) * (mLongitudeLines) * 2) + (mLongitudeLines * 2);

	mVertexCount = ((mLatitudeLines - 2) * mLongitudeLines) + 2;
	mIndexCount = numFaces * 3;*/

	mVertexCount = 24;
	mIndexCount = 36;

	vertices = new VertexPT[mVertexCount];
	if (!vertices)
		return false;

	indices = new unsigned long[mIndexCount];
	if (!indices)
		return false;

	//BACK
	vertices[0].position = VECTOR3(-1.0f, -1.0f, -1.0f);
	vertices[0].texture = VECTOR2(1.0f, 0.6666f);

	vertices[1].position = VECTOR3(-1.0f, 1.0f, -1.0f);
	vertices[1].texture = VECTOR2(1.0f, 0.3343f);

	vertices[2].position = VECTOR3(1.0f, 1.0f, -1.0f);
	vertices[2].texture = VECTOR2(0.75f, 0.3343f);

	vertices[3].position = VECTOR3(1.0f, -1.0f, -1.0f);
	vertices[3].texture = VECTOR2(0.75f, 0.6666f);

	//FRONT
	vertices[4].position = VECTOR3(-1.0f, -1.0f, 1.0f);
	vertices[4].texture = VECTOR2(0.25f, 0.6666f);

	vertices[5].position = VECTOR3(1.0f, -1.0f, 1.0f);
	vertices[5].texture = VECTOR2(0.50f, 0.6666f);

	vertices[6].position = VECTOR3(1.0f, 1.0f, 1.0f);
	vertices[6].texture = VECTOR2(0.50f, 0.3333f);

	vertices[7].position = VECTOR3(-1.0f, 1.0f, 1.0f);
	vertices[7].texture = VECTOR2(0.25f, 0.3333f);

	//TOP
	vertices[8].position = VECTOR3(-1.0f, 1.0f, -1.0f);
	vertices[8].texture = VECTOR2(0.2509f, 0.0f);

	vertices[9].position = VECTOR3(-1.0f, 1.0f, 1.0f);
	vertices[9].texture = VECTOR2(0.2509f, 0.3333f);

	vertices[10].position = VECTOR3(1.0f, 1.0f, 1.0f);
	vertices[10].texture = VECTOR2(0.4999f, 0.3333f);

	vertices[11].position = VECTOR3(1.0f, 1.0f, -1.0f);
	vertices[11].texture = VECTOR2(0.4999f, 0.0f);

	//BOTTOM
	vertices[12].position = VECTOR3(-1.0f, -1.0f, -1.0f);
	vertices[12].texture = VECTOR2(0.50f, 1.0f);

	vertices[13].position = VECTOR3(1.0f, -1.0f, -1.0f);
	vertices[13].texture = VECTOR2(0.25f, 1.0f);

	vertices[14].position = VECTOR3(1.0f, -1.0f, 1.0f);
	vertices[14].texture = VECTOR2(0.50f, 0.6666f);

	vertices[15].position = VECTOR3(-1.0f, -1.0f, 1.0f);
	vertices[15].texture = VECTOR2(0.25f, 0.6666f);

	//LEFT
	vertices[16].position = VECTOR3(-1.0f, -1.0f, 1.0f);
	vertices[16].texture = VECTOR2(0.25f, 0.6666f);

	vertices[17].position = VECTOR3(-1.0f, 1.0f, 1.0f);
	vertices[17].texture = VECTOR2(0.25f, 0.3343f);

	vertices[18].position = VECTOR3(-1.0f, 1.0f, -1.0f);
	vertices[18].texture = VECTOR2(0.0f, 0.3343f);

	vertices[19].position = VECTOR3(-1.0f, -1.0f, -1.0f);
	vertices[19].texture = VECTOR2(0.0f, 0.6666f);

	//RIGHT
	vertices[20].position = VECTOR3(1.0f, -1.0f, -1.0f);
	vertices[20].texture = VECTOR2(0.75f, 0.6666f);

	vertices[21].position = VECTOR3(1.0f, -1.0f, 1.0f);
	vertices[21].texture = VECTOR2(0.50f, 0.6666f);

	vertices[22].position = VECTOR3(1.0f, 1.0f, 1.0f);
	vertices[22].texture = VECTOR2(0.50f, 0.3343f);

	vertices[23].position = VECTOR3(1.0f, 1.0f, -1.0f);
	vertices[23].texture = VECTOR2(0.75f, 0.3343f);

	//BACK
	indices[0] = 2;
	indices[1] = 1;
	indices[2] = 0;

	indices[3] = 0;
	indices[4] = 3;
	indices[5] = 2;

	//FRONT
	indices[6] = 6;
	indices[7] = 5;
	indices[8] = 4;

	indices[9] = 7;
	indices[10] = 6;
	indices[11] = 4;

	//TOP
	indices[12] = 10;
	indices[13] = 9;
	indices[14] = 8;

	indices[15] = 11;
	indices[16] = 10;
	indices[17] = 8;

	//BOTTOM
	indices[18] = 15;
	indices[19] = 14;
	indices[20] = 13;

	indices[21] = 13;
	indices[22] = 12;
	indices[23] = 15;

	//LEFT
	indices[24] = 18;
	indices[25] = 17;
	indices[26] = 16;

	indices[27] = 19;
	indices[28] = 18;
	indices[29] = 16;

	//RIGHT
	indices[30] = 22;
	indices[31] = 23;
	indices[32] = 21;

	indices[33] = 20;
	indices[34] = 21;
	indices[35] = 23;

	/*FLOAT sphereYaw = 0.0f;
	FLOAT spherePitch = 0.0f;
	MATRIX4 rotationX;
	MATRIX4 rotationY;
	VECTOR4 currentPosition = VECTOR4(0.0f, 0.0f, 1.0f, 0.0f);

	vertices[0].position = VECTOR3(0.0f, 0.0f, 1.0f);

	for (POSITIVEINT i = 0; i < mLatitudeLines - 2; i++)
	{
	spherePitch = (i + 1) * (DirectX::XM_PI / (mLatitudeLines - 1));
	rotationX = DirectX::XMMatrixRotationX(spherePitch);

	for (POSITIVEINT j = 0; j < mLongitudeLines; j++)
	{
	sphereYaw = j * ((DirectX::XM_PI * 2) / mLongitudeLines);
	rotationY = DirectX::XMMatrixRotationZ(sphereYaw);
	currentPosition = DirectX::XMVector3TransformNormal(VECTOR4(0.0f, 0.0f, 1.0f, 0.0f), (rotationX * rotationY));
	currentPosition = DirectX::XMVector3Normalize(currentPosition);

	vertices[i * mLongitudeLines + j + 1].position = VECTOR3(currentPosition.x, currentPosition.y, currentPosition.z);
	}
	}

	vertices[mVertexCount - 1].position = VECTOR3(0.0f, 0.0f, -1.0f);

	POSITIVEINT k = 0;

	for (POSITIVEINT i = 0; i < mLongitudeLines - 1; i++)
	{
	indices[k] = 0;
	indices[k + 1] = i + 1;
	indices[k + 2] = i + 2;

	k += 3;
	}

	indices[k] = 0;
	indices[k + 1] = mLongitudeLines;
	indices[k + 2] = 1;

	k += 3;

	for (POSITIVEINT i = 0; i < mLatitudeLines - 3; i++)
	{
	for (POSITIVEINT j = 0; j < mLongitudeLines - 1; j++)
	{
	indices[k] = i * mLongitudeLines + j + 1;
	indices[k + 1] = i * mLongitudeLines + j + 2;
	indices[k + 2] = (i + 1) * mLongitudeLines + j + 1;

	indices[k + 3] = (i + 1) * mLongitudeLines + j + 1;
	indices[k + 4] = i * mLongitudeLines + j + 2;
	indices[k + 5] = (i + 1) * mLongitudeLines + j + 2;

	k += 6;
	}

	indices[k] = (i * mLongitudeLines) + mLongitudeLines;
	indices[k + 1] = (i * mLongitudeLines) + 1;
	indices[k + 2] = ((i + 1) * mLongitudeLines) + mLongitudeLines;

	indices[k + 3] = ((i + 1) * mLongitudeLines) + mLongitudeLines;
	indices[k + 4] = (i * mLongitudeLines) + 1;
	indices[k + 5] = ((i + 1) * mLatitudeLines) + 1;

	k += 6;
	}

	for (POSITIVEINT i = 0; i < mLongitudeLines - 1; i++)
	{
	indices[k] = mVertexCount - 1;
	indices[k + 1] = (mVertexCount - 1) - (i + 1);
	indices[k + 2] = (mVertexCount - 1) - (i + 2);

	k += 3;
	}

	indices[k] = mVertexCount - 1;
	indices[k + 1] = (mVertexCount - 1) - mLongitudeLines;
	indices[k + 2] = mVertexCount - 2;*/

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexPT) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	HR(device->CreateBuffer(&vertexBufferDesc, &vertexData, &mVertexBuffer));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * mIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = indices;

	HR(device->CreateBuffer(&indexBufferDesc, &indexData, &mIndexBuffer));

	Memory::SafeDeleteArr(vertices);

	Memory::SafeDeleteArr(indices);

	return true;
}

void Skybox::Render(ID3D11DeviceContext *deviceContext, MATRIX4 viewMatrix, MATRIX4 projMatrix, MATRIX4 lightViewMatrix, MATRIX4 lightProjMatrix)
{
	POSITIVEINT stride = sizeof(VertexPT);
	POSITIVEINT offset = 0;

	deviceContext->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);

	deviceContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	for (POSITIVEINT i = 0; i < mShaderVec.size(); i++)
	{
		if (mTextureArr)
			mShaderVec[i]->SetTextures(deviceContext, mTextureArr);

		mShaderVec[i]->SetMatrixBuffer(deviceContext, mWorldMatrix, viewMatrix, projMatrix, lightViewMatrix, lightProjMatrix);
		mShaderVec[i]->RenderShader(deviceContext, mIndexCount);
	}
}

void Skybox::Update(const FLOAT dt)
{
	mWorldMatrix = DirectX::XMMatrixIdentity();

	MATRIX4 translation = DirectX::XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);
	MATRIX4 scale = DirectX::XMMatrixScaling(mScale, mScale, mScale);

	mWorldMatrix = scale * translation;
}

void Skybox::Release()
{
	TSVEntity::Release();
}