#pragma once
#include "TSVEntity.h"

class Grid : public TSVEntity
{
private:
	POSITIVEINT mGridHeight;
	POSITIVEINT mGridWidth;

public:
	Grid(POSITIVEINT id, VECTOR3 position, FLOAT scale);
	~Grid();

	bool Initialize(ID3D11Device *device, VECTOR4 color, ShaderManager *shaderManager, wchar_t* shaderFileName);
	bool Initialize(ID3D11Device *device, wchar_t* fileName, TextureManager *texManager, ShaderManager *shaderManager, wchar_t* shaderFileName);
	bool InitializeBuffers(ID3D11Device *device);
	void Render(ID3D11DeviceContext *deviceContext, MATRIX4 viewMatrix, MATRIX4 projMatrix, MATRIX4 lightViewMatrix, MATRIX4 lightProjMatrix);
	void Update(const FLOAT dt);
	void Release();
};

