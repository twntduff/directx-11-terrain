#include "LightSource.h"

LightSource::LightSource(VECTOR4 ambient, VECTOR4 diffuse, VECTOR3 position, VECTOR3 target)
{
	mAmbientColor = ambient;
	mDiffuseColor = diffuse;
	mPosition = position;
	mTarget = target;
}

LightSource::~LightSource()
{

}

void LightSource::Initialize(FLOAT screenDepth, FLOAT screenNear)
{
	BuildLightViewMatrix();
	BuildLightProjectionMatrix(screenDepth, screenNear);
}

void LightSource::Update(FLOAT dt)
{
	BuildLightViewMatrix();
}

void LightSource::SetLightParameters(VECTOR4 ambient, VECTOR4 diffuse)
{
	mAmbientColor = ambient;
	mDiffuseColor = diffuse;
}

void LightSource::BuildLightViewMatrix()
{
	mLightView = DirectX::XMMatrixLookAtLH(mPosition, mTarget, VECTOR3(0.0f, 1.0f, 0.0f));
}

void LightSource::BuildLightProjectionMatrix(FLOAT screenDepth, FLOAT screenNear)
{
	FLOAT fieldOfView, screenAspect;


	// Setup field of view and screen aspect for a square light source.
	fieldOfView = (FLOAT)DirectX::XM_PI / 2.0f;
	screenAspect = 1.0f;

	// Create the projection matrix for the light.
	mLightProjection = DirectX::XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, screenNear, screenDepth);
}