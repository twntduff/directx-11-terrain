#include "Grid.h"

Grid::Grid(POSITIVEINT id, VECTOR3 position, FLOAT scale)
	:TSVEntity(id, position, scale)
{
	mGridHeight = 100;
	mGridWidth = 100;
}

Grid::~Grid()
{
	Release();
}

bool Grid::Initialize(ID3D11Device *device, VECTOR4 color, ShaderManager *shaderManager, wchar_t* shaderFileName)
{
	mColor = color;

	if (!LoadShader(shaderManager, shaderFileName))
		return false;

	if (!InitializeBuffers(device))
		return false;;

	return true;
}

bool Grid::Initialize(ID3D11Device *device, wchar_t* fileName, TextureManager *texManager, ShaderManager *shaderManager, wchar_t* shaderFileName)
{
	if (!LoadShader(shaderManager, shaderFileName))
		return false;

	if (!LoadTexture(texManager, fileName))
		return false;

	if (!InitializeBuffers(device))
		return false;

	return true;
}

bool Grid::InitializeBuffers(ID3D11Device *device)
{
	VertexPCN *vertices;
	unsigned long *indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	mVertexCount = (mGridWidth - 1) * (mGridHeight - 1) * 8;
	mIndexCount = mVertexCount;

	vertices = new VertexPCN[mVertexCount];
	if (!vertices)
		return false;

	indices = new unsigned long[mIndexCount];
	if (!indices)
		return false;

	POSITIVEINT index = 0;

	for (POSITIVEINT j = 0; j < (mGridHeight - 1); j++)
	{
		for (POSITIVEINT i = 0; i < (mGridWidth - 1); i++)
		{
			// LINE 1
			// Upper left.
			FLOAT positionX = (FLOAT)i;
			FLOAT positionZ = (FLOAT)(j + 1);

			vertices[index].position = VECTOR3(positionX, 0.0f, positionZ);
			vertices[index].color = mColor;
			indices[index] = index;
			index++;

			// Upper right.
			positionX = (FLOAT)(i + 1);
			positionZ = (FLOAT)(j + 1);

			vertices[index].position = VECTOR3(positionX, 0.0f, positionZ);
			vertices[index].color = mColor;
			indices[index] = index;
			index++;

			// LINE 2
			// Upper right.
			positionX = (FLOAT)(i + 1);
			positionZ = (FLOAT)(j + 1);

			vertices[index].position = VECTOR3(positionX, 0.0f, positionZ);
			vertices[index].color = mColor;
			indices[index] = index;

			index++;

			// Bottom right.
			positionX = (FLOAT)(i + 1);
			positionZ = (FLOAT)j;

			vertices[index].position = VECTOR3(positionX, 0.0f, positionZ);
			vertices[index].color = mColor;
			indices[index] = index;
			index++;

			// LINE 3
			// Bottom right.
			positionX = (FLOAT)(i + 1);
			positionZ = (FLOAT)j;

			vertices[index].position = VECTOR3(positionX, 0.0f, positionZ);
			vertices[index].color = mColor;
			indices[index] = index;
			index++;

			// Bottom left.
			positionX = (FLOAT)i;
			positionZ = (FLOAT)j;

			vertices[index].position = VECTOR3(positionX, 0.0f, positionZ);
			vertices[index].color = mColor;
			indices[index] = index;
			index++;

			// LINE 4
			// Bottom left.
			positionX = (FLOAT)i;
			positionZ = (FLOAT)j;

			vertices[index].position = VECTOR3(positionX, 0.0f, positionZ);
			vertices[index].color = mColor;
			indices[index] = index;
			index++;

			// Upper left.
			positionX = (FLOAT)i;
			positionZ = (FLOAT)(j + 1);

			vertices[index].position = VECTOR3(positionX, 0.0f, positionZ);
			vertices[index].color = mColor;
			indices[index] = index;
			index++;
		}
	}

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexPCN) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	HR(device->CreateBuffer(&vertexBufferDesc, &vertexData, &mVertexBuffer));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * mIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = indices;

	HR(device->CreateBuffer(&indexBufferDesc, &indexData, &mIndexBuffer));

	Memory::SafeDeleteArr(vertices);

	Memory::SafeDeleteArr(indices);

	return true;
}

void Grid::Render(ID3D11DeviceContext *deviceContext, MATRIX4 viewMatrix, MATRIX4 projMatrix, MATRIX4 lightViewMatrix, MATRIX4 lightProjMatrix)
{
	POSITIVEINT stride = sizeof(VertexPCN);
	POSITIVEINT offset = 0;

	deviceContext->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);

	deviceContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	for (POSITIVEINT i = 0; i < mShaderVec.size(); i++)
	{
		if (mTextureArr)
			mShaderVec[i]->SetTextures(deviceContext, mTextureArr);

		mShaderVec[i]->SetMatrixBuffer(deviceContext, mWorldMatrix, viewMatrix, projMatrix, lightViewMatrix, lightProjMatrix);
		mShaderVec[i]->RenderShader(deviceContext, mIndexCount);
	}
}

void Grid::Update(const FLOAT dt)
{
	mWorldMatrix = DirectX::XMMatrixIdentity();

	MATRIX4 translation = DirectX::XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);
	MATRIX4 scale = DirectX::XMMatrixScaling(mScale, mScale, mScale);

	mWorldMatrix = scale * translation;
}

void Grid::Release()
{
	TSVEntity::Release();
}