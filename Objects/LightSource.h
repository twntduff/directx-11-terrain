#pragma once
#include "DXUtility.h"

class LightSource
{
private:
	VECTOR3 mPosition;
	VECTOR3 mTarget;
	VECTOR4 mAmbientColor;
	VECTOR4 mDiffuseColor;
	MATRIX4 mLightView;
	MATRIX4 mLightProjection;

public:
	LightSource(VECTOR4 ambient, VECTOR4 diffuse, VECTOR3 position, VECTOR3 target);
	~LightSource();

	void Initialize(FLOAT screenDepth, FLOAT screenNear);
	void Update(FLOAT dt);
	void BuildLightViewMatrix();
	void BuildLightProjectionMatrix(FLOAT screenDepth, FLOAT screenNear);
	void SetLightParameters(VECTOR4 ambient, VECTOR4 diffuse);

	void SetAmbientColor(VECTOR4 ambient){ mAmbientColor = ambient; };
	void SetDiffuseColor(VECTOR4 diffuse){ mDiffuseColor = diffuse; };

	VECTOR4 GetAmbientColor(){ return mAmbientColor; };
	VECTOR4 GetDiffuseColor(){ return mDiffuseColor; };
	MATRIX4 GetLightViewMatrix(){ return mLightView; };
	MATRIX4 GetLightProjectionMatrix(){ return mLightProjection; };
};