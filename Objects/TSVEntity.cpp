#include "TSVEntity.h"

TSVEntity::TSVEntity(POSITIVEINT id, VECTOR3 position, FLOAT scale)
{
	mId = id;
	mPosition = position;

	mIndexBuffer = NULL;
	mVertexBuffer = NULL;
	mTextureArr[0] = NULL;
	mTextureArr[1] = NULL;
	mTextureArr[2] = NULL;
	mScale = scale;
	mIndexCount = 0;
	mVertexCount = 0;
	
	mWorldMatrix = DirectX::XMMatrixIdentity();
	mWorldMatrix = DirectX::XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);
}

TSVEntity::~TSVEntity()
{
	Release();
}

bool TSVEntity::LoadShader(ShaderManager *shaderManager, wchar_t* fileName)
{
	ShaderBase* shader = shaderManager->GetShader(fileName);
	
	if (!shader)
		return false;

	mShaderVec.push_back(shader);

	return true;
}

void TSVEntity::ReleaseShaders()
{
	if (!mShaderVec.empty())
	{
		for (POSITIVEINT i = 0; i < mShaderVec.size(); i++)
			mShaderVec[i]->Release();
	}
}

void TSVEntity::Release()
{
	if (mIndexBuffer)
		Memory::SafeRelease(mIndexBuffer);

	if (mVertexBuffer)
		Memory::SafeRelease(mVertexBuffer);

	ReleaseShaders();
}

bool TSVEntity::LoadTexture(TextureManager *texManager, wchar_t* fileName)
{
	mTextureArr[0] = texManager->GetTexture(fileName)->GetTexture();

	if (!mTextureArr[0])
		return false;

	return true;
}