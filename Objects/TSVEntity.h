#pragma once
#include "DXUtility.h"
#include "TextureManager.h"
#include "ShaderManager.h"

struct VertexPT{
		VECTOR3 position;
		VECTOR2 texture;
	};

struct VertexPTN{
		VECTOR3 position;
		VECTOR2 texture;
		VECTOR3 normal;
	};

struct VertexP{
		VECTOR3 position;
	};
	
struct VertexPCN{
		VECTOR3 position;
		VECTOR4 color;
		VECTOR3 normal;
	};

class TSVEntity
{
protected:
	FLOAT mScale;
	VECTOR3 mPosition;
	MATRIX4 mWorldMatrix;
	ID3D11Buffer *mIndexBuffer, *mVertexBuffer;
	POSITIVEINT	mId;
	POSITIVEINT	mIndexCount, mVertexCount;
	ID3D11ShaderResourceView *mTextureArr[3];
	VECTOR4 mColor;

	std::vector<ShaderBase*> mShaderVec;

public:
	TSVEntity(POSITIVEINT id, VECTOR3 position, FLOAT scale);
	~TSVEntity();

	virtual bool InitializeBuffers(ID3D11Device *device) = 0;
	virtual void Render(ID3D11DeviceContext *deviceContext, MATRIX4 viewMatrix, MATRIX4 projMatrix, MATRIX4 lightViewMatrix, MATRIX4 lightProjMatrix) = 0;
	virtual void Update(const FLOAT dt) = 0;
	bool LoadShader(ShaderManager *shaderManager, wchar_t* fileName);
	void ReleaseShaders();
	virtual void Release();
	bool LoadTexture(TextureManager *texManager, wchar_t* fileName);

	//Accessor
	POSITIVEINT GetIndexCount(){ return mIndexCount; };
	MATRIX4 GetWorldMatrix(){ return mWorldMatrix; };
	ID3D11ShaderResourceView**  GetTextures(){ return mTextureArr; };
	ID3D11ShaderResourceView* GetTexture(POSITIVEINT iter){ return mTextureArr[iter]; };

	//Mutator
	void SetPosition(VECTOR3 position){ mPosition = position; };
};