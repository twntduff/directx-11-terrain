
Texture2D grassTexture : register(t0);
Texture2D stoneGrassTexture : register(t1);
Texture2D stoneTexture : register(t2);

SamplerState sampleType;

struct PixelInputType{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD;
	float3 normal : NORMAL;
};

cbuffer LightBuffer2{
	float4 ambient;
	float4 diffuse;
	float3 direction;
	float padding;
};

float4 PSMain(PixelInputType input) : SV_TARGET
{
	float4 grassColor;
	float4 slopeColor;
	float4 stoneColor;
	float slope;
	float blendAmount;
	float4 texColor;
	float3 dir;
	float4 color;
	float intensity;

	grassColor = grassTexture.Sample(sampleType, input.tex);
	slopeColor = stoneGrassTexture.Sample(sampleType, input.tex);
	stoneColor = stoneTexture.Sample(sampleType, input.tex);

	slope = 1.0f - input.normal.y;

	if (slope < 0.2f)
	{
		blendAmount = slope / 0.2f;
		texColor = lerp(grassColor, slopeColor, blendAmount);
	}

	if ((slope < 0.7f) && (slope >= 0.2f))
	{
		blendAmount = (slope - 0.2f) * (1.0f / (0.7f - 0.2f));
		texColor = lerp(slopeColor, stoneColor, blendAmount);
	}

	if (slope >= 0.7f)
	{
		texColor = stoneColor;
	}

	dir = -direction;
	intensity = saturate(dot(input.normal, dir));

	color = (diffuse * intensity);

	color = saturate(color);
	color = color * texColor;

	return color;
}