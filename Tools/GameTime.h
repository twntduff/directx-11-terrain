#pragma once
#include <Windows.h>
#include "DXUtility.h"

class GameTime
{
private:
	LONGLONG mStart;
	FLOAT mFrequencySecs, mElapsedTime, mTotalTime;

public:
	GameTime(void);
	~GameTime(void);

	bool Initialize();
	void Update();

	const FLOAT GetTime() const { return mTotalTime; };
	const FLOAT GetDeltaTime() const { return mElapsedTime; };
};