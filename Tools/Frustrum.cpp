#include "Frustrum.h"

Frustrum::Frustrum(ShaderManager *mngr, ID3D11Device *device)
{

}

Frustrum::~Frustrum()
{

}

void Frustrum::ConstructFrustum(float screenDepth, MATRIX4 projection, MATRIX4 view)
{
	FLOAT minZ = -projection._43 / projection._33;
	FLOAT r = screenDepth / (screenDepth - minZ);

	projection._33 = r;
	projection._43 = -r * minZ;

	MATRIX4 temp = DirectX::XMMatrixMultiply(view, projection);

	//Near Side
	mPlanes[0].x = temp._14 + temp._11;
	mPlanes[0].y = temp._24 + temp._21;
	mPlanes[0].z = temp._34 + temp._31;
	mPlanes[0].w = temp._44 + temp._41;
	mPlanes[0] = DirectX::XMPlaneNormalize(mPlanes[0]);
	
	//Far Side
	mPlanes[1].x = temp._14 - temp._11;
	mPlanes[1].y = temp._24 - temp._21;
	mPlanes[1].z = temp._34 - temp._31;
	mPlanes[1].w = temp._44 - temp._41;
	mPlanes[1] = DirectX::XMPlaneNormalize(mPlanes[1]);

	//Left Side
	mPlanes[2].x = temp._14 - temp._12;
	mPlanes[2].y = temp._24 - temp._22;
	mPlanes[2].z = temp._34 - temp._32;
	mPlanes[2].w = temp._44 - temp._42;
	mPlanes[2] = DirectX::XMPlaneNormalize(mPlanes[2]);

	//Right Side
	mPlanes[3].x = temp._14 + temp._12;
	mPlanes[3].y = temp._24 + temp._22;
	mPlanes[3].z = temp._34 + temp._32;
	mPlanes[3].w = temp._44 + temp._42;
	mPlanes[3] = DirectX::XMPlaneNormalize(mPlanes[3]);

	//Top Side
	mPlanes[4].x = temp._13;
	mPlanes[4].y = temp._23;
	mPlanes[4].z = temp._33;
	mPlanes[4].w = temp._43;
	mPlanes[4] = DirectX::XMPlaneNormalize(mPlanes[4]);

	//Bottom Side
	mPlanes[5].x = temp._14 - temp._13;
	mPlanes[5].y = temp._24 - temp._23;
	mPlanes[5].z = temp._34 - temp._33;
	mPlanes[5].w = temp._44 - temp._43;
	mPlanes[5] = DirectX::XMPlaneNormalize(mPlanes[5]);
}

bool Frustrum::CheckPoint(VECTOR3 position)
{
	for (POSITIVEINT i = 0; i < 6; i++)
	{
		VECTOR4 temp = DirectX::XMPlaneDotCoord(mPlanes[i], position);
		
		if (temp.x < 0.0f)
			return false;
	}

	return true;
}

bool Frustrum::CheckCube(VECTOR3 center, float radius)
{
	for (POSITIVEINT i = 0; i < 6; i++)
	{
		VECTOR3 temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x - radius, center.y - radius, center.z - radius));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x + radius, center.y - radius, center.z - radius));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x - radius, center.y + radius, center.z - radius));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x + radius, center.y + radius, center.z - radius));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x - radius, center.y - radius, center.z + radius));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x + radius, center.y - radius, center.z + radius));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x - radius, center.y + radius, center.z + radius));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x + radius, center.y + radius, center.z + radius));
		if (temp.x >= 0.0f)
			continue;

		return false;
	}

	return true;
}

bool Frustrum::CheckSphere(VECTOR3 center, float radius)
{
	for (POSITIVEINT i = 0; i < 6; i++)
	{
		VECTOR4 temp = DirectX::XMPlaneDotCoord(mPlanes[i], center);
		if (temp.x < -radius)
			return false;
	}

	return true;
}

bool Frustrum::CheckRectangle(VECTOR3 center, VECTOR3 size)
{
	for (POSITIVEINT i = 0; i < 6; i++)
	{
		VECTOR3 temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x - size.x, center.y - size.y, center.z - size.z));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x + size.x, center.y - size.y, center.z - size.z));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x - size.x, center.y + size.y, center.z - size.z));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x + size.x, center.y + size.y, center.z - size.z));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x - size.x, center.y - size.y, center.z + size.z));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x + size.x, center.y - size.y, center.z + size.z));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x - size.x, center.y + size.y, center.z + size.z));
		if (temp.x >= 0.0f)
			continue;

		temp = DirectX::XMPlaneDotCoord(mPlanes[i], VECTOR3(center.x + size.x, center.y + size.y, center.z + size.z));
		if (temp.x >= 0.0f)
			continue;

		return false;
	}

	return true;
}