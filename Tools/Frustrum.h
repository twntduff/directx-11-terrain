#pragma once
#include "DXUtility.h"
#include "Cube.h"

class Frustrum
{
private:
	VECTOR4 mPlanes[6];
	
public:
	Frustrum(ShaderManager *mngr, ID3D11Device *device);
	~Frustrum();

	void ConstructFrustum(float screenDepth, MATRIX4 projection, MATRIX4 view);
	bool CheckPoint(VECTOR3 position);
	bool CheckCube(VECTOR3 center, float radius);
	bool CheckSphere(VECTOR3 center, float radius);
	bool CheckRectangle(VECTOR3 center, VECTOR3 size);
};

