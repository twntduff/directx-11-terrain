#include "DirectShow.h"


DirectShow::DirectShow(void)
{
}

DirectShow::~DirectShow(void)
{
}

bool DirectShow::Initialize()
{
	mIsPlaying = false;

	return true;
}

void DirectShow::PlayVideo(HWND hWnd, wchar_t *fileName, DirectInput* input)
{
	mIsPlaying = true;

	HR(CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC, IID_IGraphBuilder, (void **)&mGraphBuilder));

	//Finds path to file------------------------------------------------------------
	//      char buffer[MAX_PATH];
	//      string Result;
	//      GetModuleFileName( NULL, buffer, MAX_PATH );
	//      string startuppath = string(buffer);
	//string filename(fileName.begin(), fileName.end());
	//      startuppath = startuppath.substr(0, startuppath.find_last_of( "\\/" ));
	//      Result = startuppath + string("\\") + filename;
	//      const char *File_Path = Result.c_str();
	//      WCHAR *MediaFile = new WCHAR[strlen(File_Path)+1];
	//      MultiByteToWideChar(CP_ACP, 0, File_Path, -1, MediaFile, strlen(File_Path)+1);
	////-----------------------------------------------------------------------------
	HR(mGraphBuilder->QueryInterface(IID_IMediaControl, (void**)&mMediaController));
	HR(mGraphBuilder->QueryInterface(IID_IMediaEventEx, (void**)&mEvent));
	HR(mGraphBuilder->RenderFile(fileName, NULL));
	HR(mGraphBuilder->QueryInterface(IID_IVideoWindow, (void**)&mWindow));

	HR(mWindow->put_Owner((OAHWND)hWnd));
	HR(mWindow->put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS));

	RECT rect;
	GetClientRect(hWnd, &rect);

	HR(mWindow->SetWindowPosition(rect.left, rect.top, rect.right, rect.bottom));

	HR(mMediaController->Run());

	long evCode = 0;
	LONG_PTR param1;
	LONG_PTR param2;
	long timeout = INFINITE;

	while (mIsPlaying)
	{
		mEvent->GetEvent(&evCode, &param1, &param2, 0);
		input->CheckDevice(hWnd);

		if (evCode == EC_COMPLETE)
			mIsPlaying = false;
		else if (input->IsKeyPressed(DIK_ESCAPE))
			mIsPlaying = false;
			
		mEvent->FreeEventParams(evCode, param1, param2);
	}

	mMediaController->Stop();
	

	Release();
}

void DirectShow::Release()
{
	if (mGraphBuilder)
		Memory::SafeRelease(mGraphBuilder);

	if (mMediaController)
		Memory::SafeRelease(mMediaController);

	if (mWindow)
		Memory::SafeRelease(mWindow);

	if (mEvent)
		Memory::SafeRelease(mEvent);
}