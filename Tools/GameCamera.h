#pragma once
#include "DXUtility.h"
#include "DirectInput.h"

class GameCamera
{
private:
	MATRIX4 mProjectionMatrix;
	MATRIX4 mReflectionMatrix;
	MATRIX4 mInvertedPitchMatrix;
	VECTOR3 mPosition;
	VECTOR3 mPrevPosition;
	VECTOR3 mRotation;
	VECTOR3 mUp, mForward, mRight;
	VECTOR3 mLookAt;
	VECTOR2 mWindowCenter;
	MATRIX4 mViewMatrix;
	FLOAT mSpeed;
	FLOAT mSensitivity;

public:
	GameCamera();
	~GameCamera();

	void Update(FLOAT dt, DirectInput *dInput);
	bool Initialize(VECTOR3 position, HWND hWnd);
	void BuildViewMatrix();
	MATRIX4 BuildReflectionMatrix(FLOAT height);
	void BuildProjectionMatrix(HWND hWnd);
	void Rotate(FLOAT dt, DirectInput* dInput);
	void Transform(FLOAT dt, DirectInput* dInput);

	MATRIX4 GetViewMatrix(){ return mViewMatrix; };
	MATRIX4 GetReflectionMatrix(){ return mReflectionMatrix; };
	MATRIX4 GetProjectionMatrix(){ return mProjectionMatrix; };
	VECTOR3 GetPosition(){ return VECTOR3(mPosition); };
	VECTOR3 GetPreviousPosition(){ return mPrevPosition; };
	VECTOR3 GetRotation(){ return VECTOR3(mRotation); };

	void SetPosition(FLOAT x, FLOAT y, FLOAT z){ mPosition.x = x; mPosition.y = y; mPosition.z = z; };
	void SetRotation(FLOAT x, FLOAT y, FLOAT z){ mRotation.x = x; mRotation.y = y; mRotation.z = z; };
};
