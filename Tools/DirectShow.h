#pragma once
#include <dshow.h>
#include "DirectInput.h"

using namespace std;

class DirectShow
{
private:
	bool mIsPlaying;
	IGraphBuilder *mGraphBuilder;
	IMediaControl *mMediaController;
	IVideoWindow * mWindow;
	IMediaEvent *mEvent;

public:
	DirectShow(void);
	~DirectShow(void);

	bool Initialize();
	void PlayVideo(HWND hWnd, wchar_t *fileName, DirectInput* input);
	void Release();

	bool GetIsPlaying(){return mIsPlaying;};
	IGraphBuilder* GetGraphBuilder(){return mGraphBuilder;};
	IMediaControl* GetMediaController(){return mMediaController;};
};

