#pragma once
#define DIRECTINPUT_VERSION 0x0800

#include <Windows.h>
#include <string>
#include <stdlib.h>
#include <time.h>  
#include <vector>
#include <unordered_map>
#include <numeric>
#include <thread>
#include <stdio.h>
#include <dinput.h>
#include <d3d11.h>
#include <DirectXColors.h>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <d3dcompiler.h>
#include "DXError.h"
#include "SimpleMath.h"
//#include "SpriteBatch.h"
//#include "SpriteFont.h"
#include "DDSTextureLoader.h"

typedef DirectX::SimpleMath::Matrix MATRIX4;
typedef DirectX::SimpleMath::Vector3 VECTOR3;
typedef DirectX::SimpleMath::Vector2 VECTOR2;
typedef DirectX::SimpleMath::Vector4 VECTOR4;
typedef DirectX::SimpleMath::Color COLOR;
typedef unsigned int POSITIVEINT;
typedef signed int NEGATIVEINT;
typedef void* VPNTR;
typedef int WHOLENUM;
//typedef wchar_t* wchar_t*;
typedef char* CHARPTR;
typedef wchar_t* WCHARPTR;

#ifdef _DEBUG
#ifndef HR
#define HR(x) \
{ \
	HRESULT hr = x; \
	if (FAILED(hr)) \
		{ \
		DXTraceW(__FILEW__,__LINE__,hr,L#x, TRUE); \
		} \
}
#endif
#ifndef HR
#define HR(x) x;
#endif
#endif

namespace Memory{

	template <class T> 
	void SafeDelete(T &t)
	{
		if (t)
		{
			delete t;
			t = nullptr;
		}
	}

	template <class T> 
	void SafeDeleteArr(T &t)
	{
		if (t)
		{
			delete[] t;
			t = nullptr;
		}
	}

	template <class T> 
	void SafeRelease(T &t)
	{
		if (t)
		{
			t->Release();
			t = nullptr;
		}
	}
}