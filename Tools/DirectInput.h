#pragma once
#include "DXUtility.h"

enum KeyState{
	NoChange,
	Pressed,
	Held,
	Released
};

class DirectInput
{
private:
	IDirectInput8* mDInput;
	IDirectInputDevice8* mKeyboard;
	IDirectInputDevice8* mMouse;
	DIMOUSESTATE2 mMouseState;
	char mKeyBuffer[256];
	char mCurrKeyBuffer[256];
	char mLastKeyBuffer[256];
	POINT mMousePosition;

public:
	DirectInput(HINSTANCE hInst, HWND hWnd);
	~DirectInput(void);

	void CheckDevice(HWND hWnd);
	bool KeyPressed(char key){return (mKeyBuffer[key] & 0x80) != 0;};
	bool MousePressed(int button){return (mMouseState.rgbButtons[button] & 0x80) != 0;};

	KeyState GetKeyState(int key);
	bool IsKeyPressed(int key);
	bool IsKeyHeld(int key);
	bool IsKeyReleased(int key);

	POINT GetMousePos(){return mMousePosition;};
	const FLOAT GetMouseDx() const {return (FLOAT)mMouseState.lX;};
	const FLOAT GetMouseDy() const {return (FLOAT)mMouseState.lY;};
	const FLOAT GetMouseDz() const {return (FLOAT)mMouseState.lZ;};
};

