#include "GameCamera.h"

GameCamera::GameCamera()
{
	mProjectionMatrix = DirectX::XMMatrixIdentity();
	mViewMatrix = DirectX::XMMatrixIdentity();
	mReflectionMatrix = DirectX::XMMatrixIdentity();
	mInvertedPitchMatrix = DirectX::XMMatrixIdentity();

	mUp = VECTOR3(0.0f, 1.0f, 0.0f);
	mRight = VECTOR3(1.0f, 0.0f, 0.0f);
	mForward = VECTOR3(0.0f, 0.0f, 1.0f);

	mPosition = VECTOR3(0.0f, 0.0f, -5.0f);
	mPrevPosition = VECTOR3(0.0f, 0.0f, -5.0f);
	mRotation = VECTOR3(0.0f, 0.0f, 0.0f);

	mLookAt = mPosition + mForward;
	mSpeed = 100.0f;
	mSensitivity = 4.0f;

	mInvertedPitchMatrix = DirectX::XMMatrixRotationAxis(mRight, 0.0f);
}

GameCamera::~GameCamera()
{

}

bool GameCamera::Initialize(VECTOR3 position, HWND hWnd)
{
	mPrevPosition = position;
	mPosition = position;

	BuildProjectionMatrix(hWnd);
	BuildViewMatrix();

	return true;
}

void GameCamera::Update(FLOAT dt, DirectInput *dInput)
{
	mForward = DirectX::XMVector3Normalize(mForward);
	mRight = DirectX::XMVector3Normalize(mRight);

	mUp = DirectX::XMVector3Cross(mForward, mRight);
	mUp = DirectX::XMVector3Normalize(mUp);

	BuildViewMatrix();

	Rotate(dt, dInput);
	Transform(dt, dInput);

	mLookAt = mPosition + mForward;
}

void GameCamera::Rotate(FLOAT dt, DirectInput* dInput)
{
	if (dInput->MousePressed(1))
	{
		float pitch = dInput->GetMouseDy() * dt;
		float yAngle = dInput->GetMouseDx() * dt;

		pitch *= mSensitivity;
		yAngle *= mSensitivity;

		MATRIX4 rotAxis;
		rotAxis = DirectX::XMMatrixRotationAxis(mRight, pitch);
		mInvertedPitchMatrix = DirectX::XMMatrixRotationAxis(mRight, -pitch);
		mForward = DirectX::XMVector3TransformCoord(mForward, rotAxis);
		mUp = DirectX::XMVector3TransformCoord(mUp, rotAxis);

		MATRIX4 rotYAxis;
		rotYAxis = DirectX::XMMatrixRotationY(yAngle);
		mRight = DirectX::XMVector3TransformCoord(mRight, rotYAxis);
		mUp = DirectX::XMVector3TransformCoord(mUp, rotYAxis);
		mForward = DirectX::XMVector3TransformCoord(mForward, rotYAxis);

		mInvertedPitchMatrix *= rotYAxis;
	}
}

void GameCamera::Transform(FLOAT dt, DirectInput* dInput)
{
	float speedSet = mSpeed;
	if (dInput->IsKeyHeld(DIK_LSHIFT))
	{
		speedSet *= 5;
	}

	VECTOR3 direction(0.0f, 0.0f, 0.0f);

	if (dInput->IsKeyHeld(DIK_W))
	{
		direction += mForward;
	}

	if (dInput->IsKeyHeld(DIK_S))
	{
		direction -= mForward;
	}

	if (dInput->IsKeyHeld(DIK_D))
	{
		direction += mRight;
	}

	if (dInput->IsKeyHeld(DIK_A))
	{
		direction -= mRight;
	}

	if (dInput->IsKeyHeld(DIK_Q))
	{
		direction.y += 1;
	}

	if (dInput->IsKeyHeld(DIK_E))
	{
		direction.y -= 1;
	}

	direction = DirectX::XMVector3Normalize(direction);
	mPrevPosition = mPosition;
	mPosition += direction * speedSet * dt;
	mLookAt += direction * speedSet * dt;
}

void GameCamera::BuildViewMatrix()
{
	mViewMatrix = DirectX::XMMatrixLookAtLH(mPosition, mLookAt, mUp);
}

MATRIX4 GameCamera::BuildReflectionMatrix(FLOAT height)
{
	MATRIX4 refMatrix;
	VECTOR3 position = mPosition;

	position.y = -position.y + (height * 2.0f);

	VECTOR3 up = mUp;
	VECTOR3 lookAt = mLookAt;

	up = DirectX::XMVector3TransformCoord(up, mInvertedPitchMatrix);
	lookAt = DirectX::XMVector3TransformCoord(lookAt, mInvertedPitchMatrix);

	lookAt += position;

	mReflectionMatrix = DirectX::XMMatrixLookAtLH(position, lookAt, up);

	return mReflectionMatrix;
}

void GameCamera::BuildProjectionMatrix(HWND hWnd)
{
	RECT tempRect;
	GetClientRect(hWnd, &tempRect);

	FLOAT w = (FLOAT)tempRect.right;
	FLOAT h = (FLOAT)tempRect.bottom;

	mProjectionMatrix = DirectX::XMMatrixPerspectiveFovLH(DirectX::XM_PI / 4.0f, w / h, 1.0f, 50000.0f);
}