#include "QuadTree.h"

QuadTree::QuadTree()
{
	mParentNode = 0;
	mVertices = 0;

	mNumNodes = 0;
}

QuadTree::~QuadTree()
{
	Release();
}

void QuadTree::Release()
{
	if (mParentNode)
	{
		ReleaseNode(mParentNode);
		Memory::SafeDelete(mParentNode);
	}
}

bool QuadTree::Initialize(Terrain *terrain, ID3D11Device *device)
{
	VECTOR2 position;
	FLOAT width;
	
	int vertexCount = terrain->GetVertexCount();

	mTriangleCount = vertexCount / 3;

	mVertices = new VertexPTN[vertexCount];
	if (!mVertices)
		return false;

	terrain->CopyVertexArray((void*)mVertices);

	CalculateMeshDimensions(vertexCount, position, width);

	mParentNode = new QuadNode;
	if (!mParentNode)
		return false;

	mNumNodes++;
	CreateNode(mParentNode, position, width, device);

	if (mVertices)
		Memory::SafeDeleteArr(mVertices);

	return true;
}

void QuadTree::Render(Frustrum *frustrum, ID3D11DeviceContext *deviceContext, ShaderBase *shader, GameCamera *camera, Terrain *terrain, LightSource *light)
{
	mDrawCount = 0;

	shader->SetTextures(deviceContext, terrain->GetTextures());
	shader->SetMatrixBuffer(deviceContext, terrain->GetWorldMatrix(), camera->GetViewMatrix(), camera->GetProjectionMatrix(), light->GetLightViewMatrix(), light->GetLightProjectionMatrix());

	RenderNode(mParentNode, frustrum, deviceContext, shader, camera, terrain);
}

void QuadTree::CalculateMeshDimensions(POSITIVEINT vertCount, VECTOR2 &position, FLOAT &meshWidth)
{
	position.x = 0.0f;
	position.y = 0.0f;

	for (POSITIVEINT i = 0; i < vertCount; i++)
	{
		position.x += mVertices[i].position.x;
		position.y += mVertices[i].position.z;
	}

	position.x = position.x / (FLOAT)vertCount;
	position.y = position.y / (FLOAT)vertCount;

	FLOAT maxWidth = 0.0f;
	FLOAT maxDepth = 0.0f;

	FLOAT minWidth = fabsf(mVertices[0].position.x - position.x);
	FLOAT minDepth = fabsf(mVertices[0].position.z - position.x);

	FLOAT width = 0.0f;
	FLOAT depth = 0.0f;

	for (POSITIVEINT i = 0; i < vertCount; i++)
	{
		width = fabsf(mVertices[i].position.x - position.x);
		depth = fabsf(mVertices[i].position.z - position.y);

		if (width > maxWidth)
			maxWidth = width;

		if (width < minWidth)
			minWidth = width;

		if (depth > maxDepth)
			maxDepth = depth;

		if (depth < minDepth)
			minDepth = depth;
	}

	VECTOR2 maxPosition;
	maxPosition.x = (FLOAT)max(fabsf(minWidth), fabsf(maxWidth));
	maxPosition.y = (FLOAT)max(fabsf(minDepth), fabsf(maxDepth));

	meshWidth = max(maxPosition.x, maxPosition.y) * 2.0f;
}

void QuadTree::CreateNode(QuadNode *node, VECTOR2 position, FLOAT width, ID3D11Device *device)
{
	node->Position.x = position.x;
	node->Position.y = position.y;
	node->Width = width;
	node->TriangleCount = 0;
	node->VertexBuffer = 0;
	node->IndexBuffer = 0;
	node->Nodes[0] = 0;
	node->Nodes[1] = 0;
	node->Nodes[2] = 0;
	node->Nodes[3] = 0;

	POSITIVEINT numTriangles = CountTriangles(position, width);

	if (numTriangles == 0)
		return;

	FLOAT offsetX = 0.0f;
	FLOAT offsetZ = 0.0f;
	POSITIVEINT count = 0;

	POSITIVEINT maxTriangles = 10000;
	if (numTriangles > maxTriangles)
	{
		for (POSITIVEINT i = 0; i < 4; i++)
		{
			offsetX = (((i % 2) < 1) ? -1.0f : 1.0f) * (width / 4.0f);
			offsetZ = (((i % 4) < 2) ? -1.0f : 1.0f) * (width / 4.0f);

			count = CountTriangles(VECTOR2((position.x + offsetX), (position.y + offsetZ)), (width / 2.0f));
			if (count > 0)
			{
				node->Nodes[i] = new QuadNode;
				mNumNodes++;
				CreateNode(node->Nodes[i], VECTOR2((position.x + offsetX), (position.y + offsetZ)), (width / 2.0f), device);
			}
		}

		return;
	}

	node->TriangleCount = numTriangles;

	POSITIVEINT vertexCount = numTriangles * 3;

	VertexPTN *vertices = new VertexPTN[vertexCount];

	unsigned long *indices = new unsigned long[vertexCount];

	bool result;
	POSITIVEINT vertexIndex = 0;

	POSITIVEINT index = 0;
	for (POSITIVEINT i = 0; i < mTriangleCount; i++)
	{
		result = CheckTriangleContained(i, position, width);

		if (result)
		{
			vertexIndex = i * 3;

			vertices[index].position = mVertices[vertexIndex].position;
			vertices[index].texture = mVertices[vertexIndex].texture;
			vertices[index].normal = mVertices[vertexIndex].normal;
			indices[index] = index;
			index++;
			vertexIndex++;
			
			vertices[index].position = mVertices[vertexIndex].position;
			vertices[index].texture = mVertices[vertexIndex].texture;
			vertices[index].normal = mVertices[vertexIndex].normal;
			indices[index] = index;
			index++;
			vertexIndex++;

			vertices[index].position = mVertices[vertexIndex].position;
			vertices[index].texture = mVertices[vertexIndex].texture;
			vertices[index].normal = mVertices[vertexIndex].normal;
			indices[index] = index;
			index++;
		}
	}

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexPTN) * vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;
	
	D3D11_SUBRESOURCE_DATA vertexData;
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	device->CreateBuffer(&vertexBufferDesc, &vertexData, &node->VertexBuffer);

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * vertexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexData;
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	device->CreateBuffer(&indexBufferDesc, &indexData, &node->IndexBuffer);

	Memory::SafeDeleteArr(vertices);
	Memory::SafeDeleteArr(indices);
}

POSITIVEINT QuadTree::CountTriangles(VECTOR2 position, FLOAT width)
{
	POSITIVEINT count = 0;
	bool result;

	for (POSITIVEINT i = 0; i < mTriangleCount; i++)
	{
		result = CheckTriangleContained(i, position, width);

		if (result)
			count++;
	}

	return count;
}

bool QuadTree::CheckTriangleContained(POSITIVEINT index, VECTOR2 position, FLOAT width)
{
	FLOAT radius = width / 2.0f;

	POSITIVEINT vertexIndex = index * 3;

	VECTOR2 position1;
	position1.x = mVertices[vertexIndex].position.x;
	position1.y = mVertices[vertexIndex].position.z;
	vertexIndex++;

	VECTOR2 position2;
	position2.x = mVertices[vertexIndex].position.x;
	position2.y = mVertices[vertexIndex].position.z;
	vertexIndex++;

	VECTOR2 position3;
	position3.x = mVertices[vertexIndex].position.x;
	position3.y = mVertices[vertexIndex].position.z;

	FLOAT minX = min(position1.x, min(position2.x, position3.x));
	if (minX > (position.x + radius))
		return false;

	FLOAT maxX = max(position1.x, max(position2.x, position3.x));
	if (maxX < (position.x - radius))
		return false;

	FLOAT minZ = min(position1.y, min(position2.y, position3.y));
	if (minZ > (position.y + radius))
		return false;

	FLOAT maxZ = max(position1.y, max(position2.y, position3.y));
	if (maxZ < (position.y - radius))
		return false;

	return true;
}

void QuadTree::ReleaseNode(QuadNode *node)
{
	for (POSITIVEINT i = 0; i < 4; i++)
	{
		if (node->Nodes[i] != 0)
			ReleaseNode(node->Nodes[i]);
	}

	if (node->VertexBuffer)
		Memory::SafeRelease(node->VertexBuffer);

	if (node->IndexBuffer)
		Memory::SafeRelease(node->IndexBuffer);

	for (POSITIVEINT i = 0; i < 4; i++)
	{
		if (node->Nodes[i])
			Memory::SafeDelete(node->Nodes[i]);
	}
}

void QuadTree::RenderNode(QuadNode* node, Frustrum *frustrum, ID3D11DeviceContext *immediateContext, ShaderBase *shader, GameCamera *camera, Terrain *terrain)
{

	bool result = frustrum->CheckCube(VECTOR3(node->Position.x, 0.0f, node->Position.y), node->Width);

	if (!result)
		return;

	POSITIVEINT count = 0;
	for (POSITIVEINT i = 0; i < 4; i++)
	{
		if (node->Nodes[i] != 0)
		{
			RenderNode(node->Nodes[i], frustrum, immediateContext, shader, camera, terrain);
			count++;
		}
	}

	if (count != 0)
		return;

	POSITIVEINT stride = sizeof(VertexPTN);
	POSITIVEINT offset = 0;

	immediateContext->IASetVertexBuffers(0, 1, &node->VertexBuffer, &stride, &offset);

	immediateContext->IASetIndexBuffer(node->IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	immediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	POSITIVEINT indexCount = node->TriangleCount * 3;

	shader->RenderShader(immediateContext, indexCount);

	mDrawCount += node->TriangleCount;
}