#include "Terrain.h"
#include "TerrainShader.h"

Terrain::Terrain(POSITIVEINT id, VECTOR3 position, FLOAT scale)
	:TSVEntity(id, position, scale)
{
	mTerrainHeight = 0;
	mTerrainWidth = 0;

	mVertices = 0;
}

Terrain::~Terrain()
{
	Release();
}

bool Terrain::Initialize(ID3D11Device *device, VECTOR4 color, char* mapFileName, ShaderManager *shaderManager, wchar_t* shaderFileName)
{
	mColor = color;

	if (!LoadShader(shaderManager, shaderFileName))
		return false;

	if (!LoadHeightMap(mapFileName))
		return false;

	NormalizeHeightMap();

	if (!CalculateNormals())
		return false;

	if (!InitializeBuffers(device))
		return false;

	return true;
}

bool Terrain::Initialize(ID3D11Device *device, wchar_t* grassFileName, wchar_t* stoneFileName, wchar_t* stoneGrassFileName, TextureManager *texManager, char* mapFileName, ShaderManager *shaderManager, wchar_t* shaderFileName)
{
	if (!LoadShader(shaderManager, shaderFileName))
		return false;

	if(!LoadHeightMap(mapFileName))
		return false;

	NormalizeHeightMap();

	CalculateTextureCoords();

	if(!CalculateNormals())
		return false;
	
	if (!LoadTexture(texManager, grassFileName))
		return false;

	if (!LoadStoneTexture(texManager, stoneFileName))
		return false;

	if (!LoadStoneGrassTexture(texManager, stoneGrassFileName))
		return false;

	if (!InitializeBuffers(device))
		return false;

	return true;
}

bool Terrain::InitializeBuffers(ID3D11Device *device)
{
	mVertexCount = (mTerrainWidth - 1) * (mTerrainHeight - 1) * 6;

	mVertices = new VertexPTN[mVertexCount];
	if (!mVertices)
		return false;

	POSITIVEINT index = 0;
	POSITIVEINT index1 = 0;
	POSITIVEINT index2 = 0;
	POSITIVEINT index3 = 0;
	POSITIVEINT index4 = 0;

	FLOAT tu = 0.0f;
	FLOAT tv = 0.0f;

	for (POSITIVEINT j = 0; j < (mTerrainHeight - 1); j++)
	{
		for (POSITIVEINT i = 0; i<(mTerrainWidth - 1); i++)
		{
			index1 = (mTerrainHeight * i) + j;
			index2 = (mTerrainHeight * i) + (j + 1);
			index3 = (mTerrainHeight * (i + 1)) + j;
			index4 = (mTerrainHeight * (i + 1)) + (j + 1);

			tv = mHeightMap[index3].Texture.y;

			if (tv == 1.0f) 
				tv = 0.0f; 

			mVertices[index].position = VECTOR3(mHeightMap[index3].Position.x, mHeightMap[index3].Position.y, mHeightMap[index3].Position.z);
			mVertices[index].texture = VECTOR2(mHeightMap[index3].Texture.x, tv);
			mVertices[index].normal = VECTOR3(mHeightMap[index3].Normal.x, mHeightMap[index3].Normal.y, mHeightMap[index3].Normal.z);
			index++;

			tu = mHeightMap[index4].Texture.x;
			tv = mHeightMap[index4].Texture.y;

			if (tu == 0.0f) 
				tu = 1.0f; 

			if (tv == 1.0f) 
				tv = 0.0f;

			mVertices[index].position = VECTOR3(mHeightMap[index4].Position.x, mHeightMap[index4].Position.y, mHeightMap[index4].Position.z);
			mVertices[index].texture = VECTOR2(tu, tv);
			mVertices[index].normal = VECTOR3(mHeightMap[index4].Normal.x, mHeightMap[index4].Normal.y, mHeightMap[index4].Normal.z);
			index++;

			mVertices[index].position = VECTOR3(mHeightMap[index1].Position.x, mHeightMap[index1].Position.y, mHeightMap[index1].Position.z);
			mVertices[index].texture = VECTOR2(mHeightMap[index1].Texture.x, mHeightMap[index1].Texture.y);
			mVertices[index].normal = VECTOR3(mHeightMap[index1].Normal.x, mHeightMap[index1].Normal.y, mHeightMap[index1].Normal.z);
			index++;

			mVertices[index].position = VECTOR3(mHeightMap[index1].Position.x, mHeightMap[index1].Position.y, mHeightMap[index1].Position.z);
			mVertices[index].texture = VECTOR2(mHeightMap[index1].Texture.x, mHeightMap[index1].Texture.y);
			mVertices[index].normal = VECTOR3(mHeightMap[index1].Normal.x, mHeightMap[index1].Normal.y, mHeightMap[index1].Normal.z);
			index++;

			tu = mHeightMap[index4].Texture.x;
			tv = mHeightMap[index4].Texture.y;

			if (tu == 0.0f)
				tu = 1.0f;

			if (tv == 1.0f)
				tv = 0.0f;

			mVertices[index].position = VECTOR3(mHeightMap[index4].Position.x, mHeightMap[index4].Position.y, mHeightMap[index4].Position.z);
			mVertices[index].texture = VECTOR2(tu, tv);
			mVertices[index].normal = VECTOR3(mHeightMap[index4].Normal.x, mHeightMap[index4].Normal.y, mHeightMap[index4].Normal.z);
			index++;

			tu = mHeightMap[index2].Texture.x;

			if (tu == 0.0f)
				tu = 1.0f;

			mVertices[index].position = VECTOR3(mHeightMap[index2].Position.x, mHeightMap[index2].Position.y, mHeightMap[index2].Position.z);
			mVertices[index].texture = VECTOR2(tu, mHeightMap[index2].Texture.y);
			mVertices[index].normal = VECTOR3(mHeightMap[index2].Normal.x, mHeightMap[index2].Normal.y, mHeightMap[index2].Normal.z);
			index++;

		}
	}

	return true;
}

void Terrain::Render(ID3D11DeviceContext *deviceContext, MATRIX4 viewMatrix, MATRIX4 projMatrix, MATRIX4 lightViewMatrix, MATRIX4 lightProjMatrix)
{
	POSITIVEINT stride = sizeof(VertexPTN);
	POSITIVEINT offset = 0;

	deviceContext->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);

	deviceContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	for (POSITIVEINT i = 0; i < mShaderVec.size(); i++)
	{
		if (mTextureArr)
			mShaderVec[i]->SetTextures(deviceContext, mTextureArr);

		mShaderVec[i]->SetMatrixBuffer(deviceContext, mWorldMatrix, viewMatrix, projMatrix, lightViewMatrix, lightProjMatrix);
		mShaderVec[i]->RenderShader(deviceContext, mIndexCount);
	}
}

void Terrain::Update(const FLOAT dt)
{
	mWorldMatrix = DirectX::XMMatrixIdentity();

	MATRIX4 translation = DirectX::XMMatrixTranslation(mPosition.x, mPosition.y, mPosition.z);
	MATRIX4 scale = DirectX::XMMatrixScaling(mScale, mScale, mScale);

	mWorldMatrix = scale * translation;
}

void Terrain::Release()
{
	TSVEntity::Release();

	Memory::SafeDeleteArr(mHeightMap);

	Memory::SafeDeleteArr(mVertices);
}

bool Terrain::LoadStoneTexture(TextureManager *texManager, wchar_t* fileName)
{
	mTextureArr[2] = texManager->GetTexture(fileName)->GetTexture();

	if (!mTextureArr[2])
		return false;

	return true;
}

bool Terrain::LoadStoneGrassTexture(TextureManager *texManager, wchar_t* fileName)
{
	mTextureArr[1] = texManager->GetTexture(fileName)->GetTexture();

	if (!mTextureArr[1])
		return false;

	return true;
}

bool Terrain::LoadHeightMap(char* fileName)
{
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	FILE *inFile;

	int error = fopen_s(&inFile, fileName, "rb");
	if (error != 0)
		return false;

	unsigned int count = fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, inFile);
	if (count != 1)
		return false;

	count = fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, inFile);
	if (count != 1)
		return false;

	mTerrainWidth = bitmapInfoHeader.biWidth;
	mTerrainHeight = bitmapInfoHeader.biHeight;

	int imageSize = mTerrainWidth * mTerrainHeight * 3;

	unsigned char* bitmapImage = new unsigned char[imageSize];
	if (!bitmapImage)
		return false;

	fseek(inFile, bitmapFileHeader.bfOffBits, SEEK_SET);

	count = fread(bitmapImage, 1, imageSize, inFile);
	if (count != imageSize)
		return false;

	fclose(inFile);
	
	mHeightMap = new HeightMapType[mTerrainWidth * mTerrainHeight];

	unsigned int k = 0;
	unsigned int index = 0;
	unsigned char height;
	float offset = 10.0f;

	for (unsigned int i = 0; i < mTerrainWidth; i++)
	{
		for (unsigned int j = 0; j < mTerrainHeight; j++)
		{
			height = bitmapImage[k];

			index = (mTerrainHeight * i) + j;

			mHeightMap[index].Position.x = (float)j * offset;
			mHeightMap[index].Position.y = (float)height * (offset) * 4.0f;
			mHeightMap[index].Position.z = (float)i * offset;

			k += 3;
		}
	}

	Memory::SafeDeleteArr(bitmapImage);

	return true;
}

void Terrain::NormalizeHeightMap()
{
	FLOAT highestY = -1000.0f;

	for (POSITIVEINT i = 0; i < mTerrainHeight; i++)
	{
		for (POSITIVEINT j = 0; j < mTerrainWidth; j++)
		{
			//mHeightMap[(mTerrainHeight * i) + j].Position.y /= 1.0f;
			mHeightMap[(mTerrainHeight * i) + j].Position.y /= 25.0f;
		
			if (mHeightMap[(mTerrainHeight * i) + j].Position.y > highestY)
				highestY = mHeightMap[(mTerrainHeight * i) + j].Position.y;
		}
	}

	mPosition.y = -highestY;
}

bool Terrain::CalculateNormals()
{
	VECTOR3 *normals;
	
	POSITIVEINT index = 0;
	POSITIVEINT index1 = 0;
	POSITIVEINT index2 = 0;
	POSITIVEINT index3 = 0;
	
	FLOAT vertex1[3];
	FLOAT vertex2[3];
	FLOAT vertex3[3];
	FLOAT vector1[3];
	FLOAT vector2[3];
	FLOAT sum[3];
	FLOAT length = 0.0f;

	normals = new VECTOR3[(mTerrainHeight - 1) * (mTerrainWidth - 1)];

	for (POSITIVEINT i = 0; i < mTerrainHeight - 1; i++)
	{
		for (POSITIVEINT j = 0; j < mTerrainWidth - 1; j++)
		{
			index1 = (i * mTerrainHeight) + j;
			index2 = (i * mTerrainHeight) + (j + 1);
			index3 = ((i + 1) * mTerrainHeight) + j;

			vertex1[0] = mHeightMap[index1].Position.x;
			vertex1[1] = mHeightMap[index1].Position.y;
			vertex1[2] = mHeightMap[index1].Position.z;

			vertex2[0] = mHeightMap[index2].Position.x;
			vertex2[1] = mHeightMap[index2].Position.y;
			vertex2[2] = mHeightMap[index2].Position.z;

			vertex3[0] = mHeightMap[index3].Position.x;
			vertex3[1] = mHeightMap[index3].Position.y;
			vertex3[2] = mHeightMap[index3].Position.z;

			vector1[0] = vertex1[0] - vertex3[0];
			vector1[1] = vertex1[1] - vertex3[1];
			vector1[2] = vertex1[2] - vertex3[2];
			vector2[0] = vertex3[0] - vertex2[0];
			vector2[1] = vertex3[1] - vertex2[1];
			vector2[2] = vertex3[2] - vertex2[2];

			index = (i * (mTerrainHeight - 1)) + j;

			normals[index].x = (vector1[1] * vector2[2]) - (vector1[2] * vector2[1]);
			normals[index].y = (vector1[2] * vector2[0]) - (vector1[0] * vector2[2]);
			normals[index].z = (vector1[0] * vector2[1]) - (vector1[1] * vector2[0]);
		}
	}

	for (WHOLENUM i = 0; i < mTerrainHeight; i++)
	{
		for (WHOLENUM j = 0; j < mTerrainWidth; j++)
		{
			sum[0] = 0.0f;
			sum[1] = 0.0f;
			sum[2] = 0.0f;

			POSITIVEINT count = 0;

			if ((j - 1) >= 0 && (i - 1) >= 0)
			{
				index = ((i - 1) * (mTerrainHeight - 1)) + (j - 1);

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
				count++;
			}

			if ((j < (mTerrainWidth - 1)) && ((i - 1) >= 0))
			{
				index = ((i - 1) * (mTerrainHeight - 1)) + j;

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
				count++;
			}

			if (((j - 1) >= 0) && (i < (mTerrainHeight - 1)))
			{
				index = (i * (mTerrainHeight - 1)) + (j - 1);

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
				count++;
			}

			if ((j < (mTerrainWidth - 1)) && (i < (mTerrainHeight - 1)))
			{
				index = (i * (mTerrainHeight - 1)) + j;

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
				count++;
			}

			sum[0] = (sum[0] / (FLOAT)count);
			sum[1] = (sum[1] / (FLOAT)count);
			sum[2] = (sum[2] / (FLOAT)count);

			length = sqrt((sum[0] * sum[0]) + (sum[1] * sum[1]) + (sum[2] * sum[2]));

			index = (i * mTerrainHeight) + j;

			mHeightMap[index].Normal.x = (sum[0] / length);
			mHeightMap[index].Normal.y = (sum[1] / length);
			mHeightMap[index].Normal.z = (sum[2] / length);
		}
	}

	Memory::SafeDeleteArr(normals);

	return true;
}

void Terrain::CalculateTextureCoords()
{
	FLOAT texRepeat = 8.0f;

	FLOAT incValue = texRepeat / mTerrainHeight;
	FLOAT incCount = mTerrainHeight / texRepeat;

	FLOAT tuCoord = 0.0f;
	FLOAT tvCoord = 1.0f;

	POSITIVEINT tuCount = 0;
	POSITIVEINT tvCount = 0;

	for (POSITIVEINT i = 0; i < mTerrainHeight; i++)
	{
		for (POSITIVEINT j = 0; j < mTerrainWidth; j++)
		{
			mHeightMap[(mTerrainHeight * i) + j].Texture.x = tuCoord;
			mHeightMap[(mTerrainHeight * i) + j].Texture.y = tvCoord;

			tuCoord += incValue;
			tuCount++;

			if (tuCount == incCount)
			{
				tuCoord = 0.0f;
				tuCount = 0;
			}
		}

		tvCoord -= incValue;
		tvCount++;

		if (tvCount == incCount)
		{
			tvCoord = 1.0f;
			tvCount = 0;
		}
	}
}

void Terrain::CopyVertexArray(void* vertArr)
{
	memcpy(vertArr, mVertices, sizeof(VertexPTN) * mVertexCount);
}