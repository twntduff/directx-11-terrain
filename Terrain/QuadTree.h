#pragma once
#include "Terrain.h"
#include "TerrainShader.h"
#include "Frustrum.h"
#include "GameCamera.h"
#include "LightSource.h"

struct QuadNode{
		VECTOR2 Position;
		FLOAT Width;
		POSITIVEINT TriangleCount;
		ID3D11Buffer *VertexBuffer;
		ID3D11Buffer *IndexBuffer;
		QuadNode *Nodes[4];
	};

class QuadTree
{
public:
	struct VertexPTN{
		VECTOR3 position;
		VECTOR2 texture;
		VECTOR3 normal;
	};

private:
	POSITIVEINT mTriangleCount;
	POSITIVEINT mDrawCount;
	POSITIVEINT mNumNodes;
	VertexPTN *mVertices;
	QuadNode *mParentNode;

public:
	QuadTree();
	~QuadTree();

	void Release();
	bool Initialize(Terrain *terrain, ID3D11Device *device);
	void Render(Frustrum *frustrum, ID3D11DeviceContext *immediateContext, ShaderBase *shader, GameCamera *camera, Terrain *terrain, LightSource *light);
	void CalculateMeshDimensions(POSITIVEINT vertCount, VECTOR2 &position, FLOAT &width);
	void CreateNode(QuadNode *node, VECTOR2 position, FLOAT width, ID3D11Device *device);
	POSITIVEINT CountTriangles(VECTOR2 position, FLOAT width);
	bool CheckTriangleContained(POSITIVEINT index, VECTOR2 position, FLOAT width);
	void ReleaseNode(QuadNode *node);
	void RenderNode(QuadNode *node, Frustrum *frustrum, ID3D11DeviceContext *immediateContext, ShaderBase *shader, GameCamera *camera, Terrain *terrain);

	POSITIVEINT GetDrawCount(){ return mDrawCount; };
};