#pragma once
#include "TSVEntity.h"

class Terrain : public TSVEntity
{
private:
	struct HeightMapType{

		VECTOR3 Position;
		VECTOR2 Texture;
		VECTOR3 Normal;
	};

	VertexPTN *mVertices;
	HeightMapType *mHeightMap;
	POSITIVEINT mVertexCount;
	POSITIVEINT mTerrainWidth;
	POSITIVEINT mTerrainHeight;
	
public:
	Terrain(POSITIVEINT id, VECTOR3 position, FLOAT scale);
	~Terrain();

	bool Initialize(ID3D11Device *device, VECTOR4 color, char* mapFileName, ShaderManager *shaderManager, wchar_t* shaderFileName);
	bool Initialize(ID3D11Device *device, wchar_t* grassFileName, wchar_t* stoneFileName, wchar_t* stoneGrassFileName, TextureManager *texManager, char* mapFileName, ShaderManager *shaderManager, wchar_t* shaderFileName);
	bool InitializeBuffers(ID3D11Device *device);
	void Render(ID3D11DeviceContext *deviceContext, MATRIX4 viewMatrix, MATRIX4 projMatrix, MATRIX4 lightViewMatrix, MATRIX4 lightProjMatrix);
	void Update(const FLOAT dt);
	void Release();

	bool LoadStoneTexture(TextureManager *texManager, wchar_t* fileName);
	bool LoadStoneGrassTexture(TextureManager *texManager, wchar_t* fileName);
	bool LoadHeightMap(char* fileName);
	void NormalizeHeightMap();
	bool CalculateNormals();
	void CalculateTextureCoords();
	void CopyVertexArray(void* vertArr);

	POSITIVEINT GetVertexCount(){ return mVertexCount; };
	ID3D11ShaderResourceView* GetStoneTexture(){ return mTextureArr[2]; };
	ID3D11ShaderResourceView* GetStoneGrassTexture(){ return mTextureArr[1]; };
};
