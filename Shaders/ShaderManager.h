#pragma once
#include "ShaderBase.h"

class ShaderManager
{
private:
	std::unordered_map<wchar_t*, ShaderBase*> mShaders;

public:
	ShaderManager();
	~ShaderManager();

	void LoadShader(wchar_t* shaderName, ShaderBase *shader);

	ShaderBase* GetShader(wchar_t* shaderName);
};

