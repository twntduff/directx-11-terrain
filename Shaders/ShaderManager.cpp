#include "ShaderManager.h"

ShaderManager::ShaderManager()
{
}

ShaderManager::~ShaderManager()
{
}

void ShaderManager::LoadShader(wchar_t* shaderName, ShaderBase *shader)
{
	mShaders.emplace(shaderName, shader);
}

ShaderBase* ShaderManager::GetShader(wchar_t* shaderName)
{
	ShaderBase *temp = mShaders.find(shaderName)->second;

	return temp;
}