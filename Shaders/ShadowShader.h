#pragma once
#include "ShaderBase.h"

class ShadowShader : public ShaderBase
{
private:

	struct LightBuffer{
		VECTOR4 AmbientColor;
		VECTOR4 DiffuseColor;
	};

	struct LightBuffer2{
		VECTOR3 LightPosition;
		FLOAT Padding;
	};

	ID3D11SamplerState *mSampleStateWrap;
	ID3D11SamplerState *mSampleStateClamp;
	ID3D11Buffer *mLightBuffer;
	ID3D11Buffer *mLightBuffer2;

public:
	ShadowShader(WCHARPTR vsFileName, WCHARPTR psFileName);
	~ShadowShader();

	void Release();
	void SetLightMatrixBuffer(ID3D11DeviceContext *deviceContext, ID3D11ShaderResourceView* texture, ID3D11ShaderResourceView* depthMapTexture, VECTOR3 lightPosition, VECTOR4 ambientColor, VECTOR4 diffuseColor);
	bool InitializeShader(ID3D11Device *device, HWND hWnd);
	void RenderShader(ID3D11DeviceContext *deviceContext, POSITIVEINT indexCount);
};

