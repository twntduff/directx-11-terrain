#include "ShaderBase.h"

ShaderBase::ShaderBase(WCHARPTR vsFileName, WCHARPTR psFileName)
{
	mVsFileName = vsFileName;
	mPsFileName = psFileName;

	mVertexShader = nullptr;
	mPixelShader = nullptr;
	mInputLayout = nullptr;
	mMatrixBuffer = nullptr;
}

ShaderBase::~ShaderBase()
{
	Release();
}

void ShaderBase::Release()
{
	if (mMatrixBuffer)
		Memory::SafeRelease(mMatrixBuffer);

	if (mInputLayout)
		Memory::SafeRelease(mInputLayout);

	if (mPixelShader)
		Memory::SafeRelease(mPixelShader);

	if (mVertexShader)
		Memory::SafeRelease(mVertexShader);
}

void ShaderBase::SetMatrixBuffer(ID3D11DeviceContext *immediateContext, MATRIX4 worldMatrix, MATRIX4 viewMatrix, MATRIX4 projectionMatrix, MATRIX4 lightViewMatrix, MATRIX4 lightProjMatrix)
{
	//Setup shader parameters
	worldMatrix = DirectX::XMMatrixTranspose(worldMatrix);
	viewMatrix = DirectX::XMMatrixTranspose(viewMatrix);
	projectionMatrix = DirectX::XMMatrixTranspose(projectionMatrix);

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	immediateContext->Map(mMatrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	MatrixBuffer *dataPtr = (MatrixBuffer*)mappedResource.pData;
	dataPtr->World = worldMatrix;
	dataPtr->View = viewMatrix;
	dataPtr->Projection = projectionMatrix;
	dataPtr->LightView = lightViewMatrix;
	dataPtr->LightProjection = lightProjMatrix;

	immediateContext->Unmap(mMatrixBuffer, 0);

	POSITIVEINT bufferNumber = 0;
	immediateContext->VSSetConstantBuffers(bufferNumber, 1, &mMatrixBuffer);
}