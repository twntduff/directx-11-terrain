#pragma once
#include "DXUtility.h"

class ShaderBase
{
protected:

	struct MatrixBuffer{
		MATRIX4 World;
		MATRIX4 View;
		MATRIX4 Projection;
		MATRIX4 LightView;
		MATRIX4 LightProjection;
	};

	WCHARPTR mPsFileName;
	WCHARPTR mVsFileName;

	ID3D11VertexShader *mVertexShader;
	ID3D11PixelShader *mPixelShader;
	ID3D11InputLayout *mInputLayout;
	ID3D11Buffer *mMatrixBuffer;

public:
	ShaderBase(WCHARPTR vsFileName, WCHARPTR psFileName);
	~ShaderBase();

	virtual void Release();
	
	virtual void SetTextures(ID3D11DeviceContext *deviceContext, ID3D11ShaderResourceView *textures[]) = 0;
	virtual void SetMatrixBuffer(ID3D11DeviceContext *immediateContext, MATRIX4 worldMatrix, MATRIX4 viewMatrix, MATRIX4 projectionMatrix, MATRIX4 lightViewMatrix, MATRIX4 lightProjMatrix);
	virtual bool InitializeShader(ID3D11Device *device, HWND hWnd) = 0;
	virtual void RenderShader(ID3D11DeviceContext *immediateContext, POSITIVEINT indexCount) = 0;
};