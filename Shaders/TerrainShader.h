#pragma once
#include "ShaderBase.h"

class TerrainShader : public ShaderBase
{
protected:

	struct LightBuffer{
		VECTOR4 AmbientColor;
		VECTOR4 DiffuseColor;
		VECTOR3 LightDirection;
		FLOAT Padding;
	};

private:
	ID3D11SamplerState *mSamplerState;
	ID3D11Buffer *mLightBuffer;

public:
	TerrainShader(WCHARPTR vsFileName, WCHARPTR psFileName);
	~TerrainShader();

	void Release();
	void SetTextures(ID3D11DeviceContext *deviceContext, ID3D11ShaderResourceView *textures[]);
	void SetLightBuffer(ID3D11DeviceContext *immediateContext, VECTOR4 ambient, VECTOR4 diffuse, VECTOR3 direction);
	bool InitializeShader(ID3D11Device *device, HWND hWnd);
	void RenderShader(ID3D11DeviceContext *immediateContext, POSITIVEINT indexCount);
};