#pragma once
#include "ShaderBase.h"

class TextureShader : public ShaderBase
{
private:
	ID3D11SamplerState *mSamplerState;

public:
	TextureShader(WCHARPTR vsFileName, WCHARPTR psFileName);
	~TextureShader();

	void Release();
	void SetTextures(ID3D11DeviceContext *deviceContext, ID3D11ShaderResourceView *textures[]);
	bool InitializeShader(ID3D11Device *device, HWND hWnd);
	void RenderShader(ID3D11DeviceContext *immediateContext, POSITIVEINT indexCount);
};