#pragma once
#include "DXUtility.h"

class RenderTexture
{
private:
	ID3D11Texture2D* mRenderTargetTexture;
	ID3D11RenderTargetView* mRenderTargetView;
	ID3D11ShaderResourceView* mShaderResourceView;
	ID3D11Texture2D* mDepthStencilBuffer;
	ID3D11DepthStencilView* mDepthStencilView;
	D3D11_VIEWPORT mViewport;
	MATRIX4 mProjectionMatrix;
	MATRIX4 mOrthoMatrix;

public:
	RenderTexture();
	~RenderTexture();

	bool Initialize(ID3D11Device* device, POSITIVEINT textureWidth, POSITIVEINT textureLength, FLOAT screenDepth, FLOAT screenNear);
	void Shutdown();
	void SetRenderTarget(ID3D11DeviceContext* deviceContext);
	void ClearRenderTarget(ID3D11DeviceContext* deviceContext, FLOAT red, FLOAT green, FLOAT blue, FLOAT alpha);
	
	ID3D11ShaderResourceView* GetShaderResourceView(){ return mShaderResourceView; };
	MATRIX4 GetProjectionMatrix(){ return mProjectionMatrix; };
	MATRIX4 GetOrthoMatrix(){ return mOrthoMatrix; };

};