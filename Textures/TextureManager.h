#pragma once
#include "Texture2D.h"

class TextureManager
{
private:
	std::unordered_map<wchar_t*, DXTexture2D*> mTextures;

public:
	TextureManager();
	~TextureManager();

	void Load2DTexture(wchar_t* fileName, ID3D11Device* device);
	void LoadCubeTexture(wchar_t* fileName, ID3D11Device* device);

	DXTexture2D* GetTexture(wchar_t* fileName);
};