#include "TextureManager.h"

TextureManager::TextureManager()
{

}

TextureManager::~TextureManager()
{
	
}

void TextureManager::Load2DTexture(wchar_t* fileName, ID3D11Device* device)
{
	ID3D11Resource* rsrce = nullptr;
	ID3D11ShaderResourceView* texture = nullptr;
	UINT width, height;

	HRESULT result = DirectX::CreateDDSTextureFromFile(device, fileName, &rsrce, &texture);

	D3D11_RESOURCE_DIMENSION dim;
	rsrce->GetType(&dim);

	switch (dim)
	{
	case D3D11_RESOURCE_DIMENSION_TEXTURE2D:
	{
		D3D11_TEXTURE2D_DESC desc;
		auto texture = reinterpret_cast<ID3D11Texture2D*>(rsrce);

		texture->GetDesc(&desc);
		width = desc.Width; //width of texture in pixels
		height = desc.Height; //height of texture in pixels
	}break;

	default:
	{
		width = 0; //width of texture in pixels
		height = 0; //height of texture in pixels
	}
	}

	DXTexture2D* temp = new DXTexture2D(fileName, rsrce, texture, width, height);

	mTextures.emplace(fileName,temp);
}

void TextureManager::LoadCubeTexture(wchar_t* fileName, ID3D11Device* device)
{
	ID3D11Resource* rsrce = nullptr;
	ID3D11ShaderResourceView* texture = nullptr;
	UINT width, height;

	HRESULT result = DirectX::CreateDDSTextureFromFile(device, fileName, &rsrce, &texture , 0);

	D3D11_TEXTURE2D_DESC desc;
	auto tex = reinterpret_cast<ID3D11Texture2D*>(rsrce);

	tex->GetDesc(&desc);
	width = desc.Width; //width of texture in pixels
	height = desc.Height; //height of texture in pixels
		
	D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;

	viewDesc.Format = desc.Format;
	viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	viewDesc.TextureCube.MipLevels = desc.MipLevels;
	viewDesc.TextureCube.MostDetailedMip = 0;

	HR(device->CreateShaderResourceView(rsrce, &viewDesc, &texture));

	DXTexture2D* temp = new DXTexture2D(fileName, rsrce, texture, width, height);

	mTextures.emplace(fileName, temp);
}

DXTexture2D* TextureManager::GetTexture(wchar_t* fileName)
{
	DXTexture2D* temp = mTextures.find(fileName)->second;

	return temp;
}