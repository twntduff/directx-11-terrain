#include "Texture2D.h"

DXTexture2D::DXTexture2D(const wchar_t* fileName, ID3D11Resource* rsrce, ID3D11ShaderResourceView* texture, UINT width, UINT height)
{
	t_Resource = rsrce;
	t_Texture = texture;
	t_Width = width;
	t_Height = height;
}

DXTexture2D::~DXTexture2D()
{
	Memory::SafeRelease(t_Resource);
	Memory::SafeRelease(t_Texture);
}

void DXTexture2D::Release()
{
	Memory::SafeRelease(t_Resource);
	Memory::SafeRelease(t_Texture);
}