#pragma once
#include "DXUtility.h"

class DXTexture2D
{
private:
	ID3D11Resource*				t_Resource;
	ID3D11ShaderResourceView*	t_Texture;
	UINT						t_Width;
	UINT						t_Height;
	wchar_t*					t_FileName;

public:
	DXTexture2D(const wchar_t* fileName, ID3D11Resource* rsrce, ID3D11ShaderResourceView* texture, UINT width, UINT height);
	~DXTexture2D();

	void Release();

	//Accessors
	ID3D11Resource* GetResource() const { return t_Resource; };
	ID3D11ShaderResourceView* GetTexture() const { return t_Texture; };
	const UINT GetWidth() const { return t_Width; };
	const UINT GetHeight() const { return t_Height; };
};

